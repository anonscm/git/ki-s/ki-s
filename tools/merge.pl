#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);

my $pwd = dirname(realpath($0));

require "$pwd/reptools.pm";

my ($inputlist,$output,$h);

GetOptions
(
	"li=s" => \$inputlist,
	"out=s" => \$output,
	"help"	=> \$h
);


if ( $h )
{
	&help();
	exit;
}

unless ($inputlist and $output)
{
	print("Missing arguments. .. ¯\\_(ツ)_/¯ \n Exiting.\n");
	exit;
}

&merge_files ($inputlist,$output);


=head2	function help

	Title			: help
	Usage			: &help()
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." -Merges the inputs file listed. \n"); 
	print("\n");
	print("\t-li=\t\t<txt file>\t\t\tThe input list\n");
	print("\t-out=\t\t<tab file>\t\t\tThe merging result\n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n\n");
	print("\t--help\t\t\t\t\t\tDisplay this help\n");
	print("\n");

	print("Example\n");
	
	print("\t>\tperl merge.pl -li=list.txt -out=grps.tab \n\n");
}


