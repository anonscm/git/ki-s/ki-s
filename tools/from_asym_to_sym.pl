#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);

my $pwd = dirname(realpath($0));
require "$pwd/tools.pm";

my ($inputname,$outputname,$h);

GetOptions
(
	"in=s" => \$inputname,
	"out=s" => \$outputname,
	"help" => \$h,
);

if ($h)
{
	&help();	
	exit;
}

unless ($inputname and $outputname)
{
	print("Missing arguments.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	exit;
}

&to_sym_mat($inputname,$outputname);
exit;



=head2	function help

	Title			: help
	Usage			: &help()
	Prerequisite		: none
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." - Transforms an asymetric matrix into a symetric matrix by computing the average between each a_ij and a_ji.\n"); 
	print("\n");
	print("\t-in=\t\t\t<csv file>\t\t\tThe input matrix\n");
	print("\t-out=\t\t\t<csv file>\t\t\tThe output distance matrix\n");
	print("\n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n\n");
	print("\t--help\t\t\tDisplay help\n");
	print("\n");

	print("Example\n");
	
	print("\t>\tperl from_asym_to_sym.pl -in=mat_asym.csv -out=mat_sym.csv\n\n");
	

}

