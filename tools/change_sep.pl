#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);


my $pwd = dirname(realpath($0));
require "$pwd/tools.pm";


my ($inputname,$sep1,$outputname,$sep2,$h);

GetOptions
(
	"in=s" => \$inputname,
	"sep1=s" => \$sep1,
	"out=s" => \$outputname,
	"sep2=s" => \$sep2,
	"help" => \$h
);

if ($h)
{	
	&help();
	exit;
}


unless ($inputname and $outputname and $sep1 and $sep2)
{
	print("Missing arguments.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	exit;
}



&change_separator ($inputname,$outputname,$sep1,$sep2);

exit;

=head2	function help

	Title			: help
	Usage			: &help()
	Prerequisite	: none
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." - Changes the separator of the input file\n"); 
	print("\n");
	print("\t-in=\t\t\t<csv file>\t\t\tThe input matrix\n");
	print("\t-sep1=\t\t\t<char>\t\t\t\tThe separator of the input file\n");
	print("\t-out=\t\t\t<tab file>\t\t\tThe output matrix\n");
	print("\t-sep2=\t\t\t<char>\t\t\t\tThe separator of the output file\n");
	print("\n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n\n");
	print("\t--help\t\t\tDisplay help\n");
	print("\n");

	print("Example\n");
	
	print("\t>\tperl tools/change_sep.pl -in=mat.csv -sep1=';' -out=mat.tab -sep2='\\t' \n\n");
	

}

