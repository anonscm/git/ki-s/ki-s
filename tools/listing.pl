#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);

my $pwd = dirname(realpath($0));

require "$pwd/tools.pm";

my ($inputname,$listname,$h);

GetOptions
(
	"in=s" => \$inputname,
	"li=s" => \$listname,
	"help" => \$h
);


if ($h)
{
	&help();
	exit;
}

unless ($inputname and $listname)
{
	print("Missing arguments.. ¯\\_(ツ)_/¯ \n  Exiting.\n");
	exit;
}

&listing ($inputname,$listname);

=head2	function help

	Title			: help
	Usage			: &help()
	Prerequisite		: none
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." - Lists the files from the given archive or directory into an output file.\n"); 
	print("\n");
	print("\t-in=\t\t<file|dir>\t\tThe input filename or dirname\n");
	print("\t-li=\t\t<txt file>\t\tThe output list\n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n");
	print("\t--help\t\t\tDisplay this help\n");
	print("\n");

	print("Example\n");
	
	print("\t>\tperl listing.pl -in=examples/fasta-6.zip -out=list_fasta6.csv\n\n");
}


