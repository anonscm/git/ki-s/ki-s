#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);


my $pwd = dirname(realpath($0));
my $wd = getcwd();	
require "$pwd/reptools.pm";


my ($in,$out,$clr,$color,$outputdir,$h);			


my $flatToHierarchy = "$pwd/flat_to_hierarchy.js";
my $binjs = 'nodejs';

GetOptions
(
	"in=s" => \$in,							# tab file input
	"out=s" => \$out,						# json file output
	"outdir=s" => \$outputdir,	# output dir
	"clr=s" => \$clr,						# tab file input
	"help" => \$h							# show help
);


if ($h)
{
	&help();
	exit;
}

unless ($in and $out)
{
	print("\nMissing arguments.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	print("See help with `perl ".basename($0)." --help` \n\n");
	exit;
}
#print $out."\n";
my $rh_c;

unless ( $color )
{
	$color = 5;
}


my ($rh_h, $rh_g, $ra_elements) = &get_groups ($in);

($rh_c) = &get_colors ($clr);

my $flat = $outputdir."/flat.json";
&create_flat_json ($flat,$rh_h, $rh_g, $rh_c);

chdir($outputdir) or die "$!";
my $hierarchical = `$binjs $flatToHierarchy flat.json`;

open (my $fh_output, ">".$out);
print $fh_output $hierarchical;
close ($fh_output);
chdir($wd) or die "$!";

exit;


=head2	function help

	Title			: help
	Usage			: &help()
	Prerequisite		: none
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." - Creates a JSON given a TAB file of groups and a TAB file of colors.\n"); 
	print("\n");
	print("\t-in=\t\t\t<tab file>\t\t\tThe input groups file\n");
	print("\t-out=\t\t\t<json file>\t\t\tThe output hierarchy file\n\n");
	print("\t-clr=\t\t\t<tab file>\t\t\tThe input colors file\n");

	
	print("\n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n\n");
	print("\t--help\t\t\tDisplay help\n");
	print("\n");

	print("Examples\n");
	
	print("\t>\tperl to_json.pl -in=grps.tab -clr=colors.tab -out=grps.json\n\n");

}

