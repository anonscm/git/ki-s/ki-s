#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);


my $pwd = dirname(realpath($0));
my $wd = getcwd();	

require "$pwd/reptools.pm";


my ($in,$o,$car,$from, $to,$step,$thrs,$v,$h);			

my @c = ("A".."Z","a".."z",0..9);
my $tmpSuffix = join("",@c[map{rand @c}(1..8)]);
my $tmp = "$wd/$tmpSuffix";


# scripts

my $toJson = "$pwd/to_json.pl";
my $adj = "$pwd/adj_mat.pl";
my $merge = "$pwd/merge.pl";
my $connectivity = "$pwd/composante_connexe.r";
my $char2col = "$pwd/from_characteristics_to_colors.pl";
#my $petri = "$pwd/../representations/petri_dish.html";
my $gen_visualisation = "$pwd/generate_visualisation_packing.pl";

GetOptions
(
	"in=s" => \$in,							# csv input matrix
	"from=s" => \$from,						# inf bound threshold 'from' (included)
	"to=s" => \$to,							# sup bound threshold 'to' (included)
	"step=s" => \$step,						# step
	"thrs=s" => \$thrs,						# discrete threshold
	"car=s" => \$car,						# tab file
	"v" => \$v,								# verbose mode
	"o=s" => \$o,							# prefix for the output files
	"help" => \$h							# show help
);



if ($h)
{
	&help();
	exit;
}

unless ($in and $car and $o) 
{
	print("\nMissing arguments.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	print("See help with `perl ".basename($0)." --help` \n\n");
	exit;
}

if ( $o eq "tools" or $o eq "ext" or $o eq "representations" )
{
	print("\nValue for option '-o' '$o' forbidden.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	exit;
}

### SET OUTPUT
my $outputdir = "$o";

if ( -d $outputdir )
{
  print "Output directory " . $outputdir . " already exists. Supress it or choose another output directory\n";
  exit;
#	&remove_dir($outputdir);
}

&mkdir($outputdir);


my $output = "$outputdir/" . basename($o) . ".html" ;
#`cp $petri $outputdir/$o.html` ;

my $out = "$tmpSuffix.json";
my $legendthrs = "$outputdir/$tmpSuffix\_legend\_thrs.json";
my $clr = "$outputdir/$tmpSuffix\_colors.tab";
my $legend = "$outputdir/$tmpSuffix\_legend.json";

### END SET


my (@a_thresholds) = ();

unless (($from and $to and $step) or ($thrs))
{
	print("\nMissing arguments.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	print("See help with `perl ".basename($0)." --help` \n\n");
	exit;
}

if ( $v )
{
	print("\nRetrieving the thresholds...");
}
if ($from and $to and $step)
{
	if ( $from < $to )
	{
		my $i = $from ;
#		my $upper = $to + $step;
#		while ( $i < $upper)
    while ( $i < $to)
		{
			push (@a_thresholds, $i);
			$i = $i + $step;
		}
	}
	else
	{
		print("\nUpper and lower bounds are incorrects ¯\\_(ツ)_/¯ \n Exiting.\n\n");
		exit;
	}

}
elsif ( $thrs )
{
	@a_thresholds = split (';', $thrs);
	@a_thresholds = sort (@a_thresholds);

}


#### creating threshold legend
my @a_lines = ();
my $n = 1;

foreach my $threshold (@a_thresholds)
{
	push (@a_lines,"{\"threshold\":\"$threshold\", \"color\":$n}"); 
	$n = $n + 1 ;
}
open (my $fh_output_legthrs,">".$legendthrs);
my $m = scalar(@a_lines) -1;
print $fh_output_legthrs "[\n";
for (my $i = 0; $i < $m; ++$i)
{
	print $fh_output_legthrs "\t".$a_lines[$i].",\n" ;
}
print $fh_output_legthrs "\t".$a_lines[$m]."\n";
print $fh_output_legthrs "]\n";
close ($fh_output_legthrs);



if ( $v )
{
	print ("done!\n\n");
	print ("Thresholds are: { ");
	foreach my $threshold (@a_thresholds)
	{
		print ("$threshold ");
	}
	print ("}\n\n");
}


unless ( -d $tmp )
{
	&mkdir($tmp);
}



### creating colors file and associated legend




if ( $v )
{
	print ("Creating colors file '$clr' and associated legend '$legend' with groups file '$car'...");
}

`perl $char2col -in=$car -clr=$clr -leg=$legend`;

if ( $v )
{
	print ("done!\n\n");
}

### creating cluster with r script
if ( $v )
{
	print("Clusterizing '$in' with the script '$connectivity'...");
}

my $listgroups = "$wd/$tmpSuffix\_list.txt";

if ( $v )
{
	print("The files of clusters are listed in '$listgroups'.\n\n");
}

open (my $fh_output,">".$listgroups);

foreach my $thr (@a_thresholds)
{
	
	my $filename = $tmp."/clusters\_$thr.tab";
	print $fh_output $filename."\n";	
	
	my $suff = $thr*100;
	my $adjmat = "$tmp/adj\_$suff.csv";
	if ( $v )
	{
		print("Constructs the adjacency matrix '$adjmat' with a threshold of $thr with the script '$adj'...");
	}
	
	`perl $adj -in=$in -out=$adjmat -t=$thr`;
	
	if ( $v )
	{
		print("done!\n\n");
		print("Constructs the group file '$filename' for the adjacency matrix...");
	}
	
	`Rscript $connectivity $adjmat $filename`;
	
	if ( $v )
	{
		print ("done!\n\n");
	}
}
close ($fh_output);



### merges files

my $outmerged = "$tmp/clusters_merged.tab";

if ( $v )
{
	print ("Merging the files in '$outmerged'...\n");
}

`perl $merge -li=$listgroups -out=$outmerged`;

if ( $v )
{
	print ("done!\n\n");
}

if ( $v )
{
	print ("Generating the hierarchy into '$out' with the script '$toJson' with the merged groups '$outmerged' and color file '$clr' ...");
}

#`perl $toJson -in=$outmerged -clr=$clr -out=$out`;
system "perl $toJson -in=$outmerged -clr=$clr -out=$out -outdir=$outputdir";

if ( $v )
{
	print ("done!\n\n");
}

&mv($listgroups,$outputdir);
&mv($tmp,$outputdir);

system "perl $gen_visualisation -hi=$outputdir/$out -car=$legend -thrs=$legendthrs -out=$output";

exit;

=head2	function help

	Title			: help
	Usage			: &help()
	Prerequisite		: none
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." - Constructs a JSON hierarchy with a CSV matrix, a characteristics file and thresholds, returns a directory named  after the -o option and a HTML file named after -o option with the extension .html in the working directory\n"); 
	print("\n");
	print("\t-in=\t\t\t<csv file>\t\tThe input matrix\n");
	print("\t-car=\t\t\t<tab file>\t\tThe input characteristics file\n\n");
	print("\t-o=\t\t\t<string>\t\tThe prefix of the output files\n\n");
	print("\t-from=\t\t\t<float>\t\t\tThe input lower bound for the threshold\n");
	print("\t-to=\t\t\t<float>\t\t\tThe input upper bound for the threshold\n");
	print ("\t-step=\t\t\t<float>\t\t\tThe step from the inf to the sup threshold\n");
	print("\t\tOR\n");
	print("\t-thrs=\t\t\t'<float>;<float>;...'\tA discrete input for the thresholds (order does not matter)\n");
	
	print("\n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n\n");
	print("\t--help\t\t\tDisplay help\n");
	print("\t-v \t\t\tVerbose mode\n");
	print("\n");

	print("Examples\n");
	
	print("\t>\tperl generate_packing.pl -in=mat.csv -thrs='0.95;0.99' -o=output -car=groups.tab \n\n");
	print("\t>\tperl generate_packing.pl -in=mat.csv -from=0.95 -to=0.99 -step=0.01 -o=output -car=groups.tab \n\n");

}
