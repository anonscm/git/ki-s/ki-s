var fs = require ('fs');
var path = require ('path');
var http = require ('http');

const args = process.argv;

var filename = args[2];
var on = args[3]

var parentDir = path.resolve(process.cwd(), '.');

//console.log(parentDir);
//var json = ( on == "dir" ? require(parentDir+filename) : require(filename) );
var json = require(parentDir+'/'+filename) ;


function jsonToArray (json)
{
	var res = [];
	var keys = Object.keys (json);
	keys.forEach (function (key) { res.push (json[key]); });
	return (res);
}

JSON.safeParse = function (input, def)
{	
	if (!input)
	{
		return def || {};
	}
	else
		if (Object.prototype.toString.call (input) === '[object Object]' )
		{
			return input;
		}
	
	try 
	{
		return (JSON.parse (input));
	}
	catch (e)
	{
		return def || {};	
	
	}

}

// converti un JSON Array 'plat' en JSON Array 'hiérarchique'
function convert (array)
{
    var map = {}
    for(var i = 0; i < array.length; i++)
    {
        var obj = array[i]
        if(!(obj.id in map))
        {
            map[obj.id] = obj
        }

        if(typeof map[obj.id].name == 'undefined')
        {
            map[obj.id].id = obj.id
            map[obj.id].name = obj.name
            map[obj.id].parent = obj.parent
            map[obj.id].names = obj.names
        }

        var parent = obj.parent || '-';
        if(!(parent in map))
        {
            map[parent] = {}
            map[parent].children = []
        }

        map[parent].children.push(map[obj.id])
    }
    return map['-']
}

var array = jsonToArray (json);

console.log (JSON.stringify(convert (array)));
