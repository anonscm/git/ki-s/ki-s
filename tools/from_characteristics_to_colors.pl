#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);


my $pwd = dirname(realpath($0));
require "$pwd/reptools.pm";

my $wd = getcwd();	
my ($in,$clr,$legend,$h,$v);			

my $scriptTab2Json = "$pwd/from_tab_to_json.pl";

GetOptions
(
	"in=s" => \$in,							# tab file input
	"clr=s" => \$clr,						# tab file output 
	"leg=s" => \$legend,					# json file output
	"help" => \$h,							# show help
	"v"	=> \$v								# verbose
);


if ($h)
{
	&help();
	exit;
}

unless ($in and $clr and $legend)
{
	print("\nMissing arguments.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	print("See help with `perl ".basename($0)." --help` \n\n");
	exit;
}


if ($v)
{
	print("Generating colors file '$clr' for '$in' with legend '$legend'\n\n");
}


my ($rh_colors) = &get_colors ($in);
my @a_values = ();


foreach my $key (keys %{$rh_colors})
{
	my $value = $rh_colors->{$key};
	if ( ! grep { $_ eq $value } @a_values )
	{
		push (@a_values,$value);
	}
}

@a_values = sort(@a_values);

my %h_legend ;

my $i = 1;
foreach my $value (@a_values)
{
	$h_legend{$value} = $i;
	++$i;
}

if ( $v )
{
	print ("Creating the legend '$legend'...");
}

my $tmp = $wd."/tmp\_legend.tab";
open (my $fh_output_leg, ">$tmp");

print $fh_output_leg "\tlegend\n";
foreach my $value (@a_values)
{
	print $fh_output_leg $value."\t".$h_legend{$value}."\n";
}
close ($fh_output_leg);


`perl $scriptTab2Json -in=$tmp -out=$legend`;

&remove_file ($tmp);

if ( $v )
{
	print ("done!\n\n");
}

if ( $v ) 
{
	print ("Constructs the color file '$clr'...");
}

open (my $fh_output_clr, ">$clr");

print $fh_output_clr "\tcolors\n";

foreach my $key (keys %{$rh_colors})
{
	my $value = $rh_colors->{$key};
	print $fh_output_clr $key."\t".$h_legend{$value}."\n";
}
close ($fh_output_clr);

if ( $v )
{
	print ("done!\n\n");
}

exit;




=head2	function help

	Title			: help
	Usage			: &help()
	Prerequisite		: none
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." - Constructs a colors file and its legend for a given groups file.\n"); 
	print("\n");
	print("\t-in=\t\t\t<tab file>\t\t\tThe input groups file\n");
	print("\t-clr=\t\t\t<tab file>\t\t\tThe output colors file\n");
	print("\t-leg=\t\t\t<json file>\t\t\tThe output legend file\n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n\n");
	print("\t--help\t\t\tDisplay help\n");
	print("\t-v \t\t\tVerbose mode\n");
	print("\n");

	print("Example\n");
	
	print("\t>\tperl from_groups_to_colors.pl -in=grps.tab -clr=colors.tab -leg=legend.json -v\n\n");
	

}

