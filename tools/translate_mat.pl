#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);

my $pwd = dirname(realpath($0));

require "$pwd/tools.pm";

my ($inputname,$outputname,$factor,$h);

GetOptions
(
	"in=s" => \$inputname,
	"out=s" => \$outputname,
	"fac=s" => \$factor,
	"help"	=> \$h
);


if ( $h )
{
	&help();
	exit;
}

unless ($inputname and $outputname and $factor)
{
	print("Missing arguments. .. ¯\\_(ツ)_/¯ \n Exiting.\n");
	exit;
}

&translate_mat ($inputname,$outputname,$factor);


=head2	function help

	Title			: help
	Usage			: &help()
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." - Translates the input matrix by a given factor. \n"); 
	print("\n");
	print("\t-in=\t\t<csv file>\t\t\tThe input matrix\n");
	print("\t-out=\t\t<csv file>\t\t\tThe output matrix\n");
	print("\t-fac=\t\t<float>\t\t\t\tThe factor of translation\n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n\n");
	print("\t--help\t\t\tDisplay this help\n");
	print("\n");

	print("Example\n");
	
	print("\t>\tperl translate_mat.pl -in=fasta6.csv -out=fasta6_0.01.csv -fac=0.01 \n\n");
}


