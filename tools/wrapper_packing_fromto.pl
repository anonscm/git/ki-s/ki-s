#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);


my $pwd = dirname(realpath($0));
my $wd = getcwd();	

require "$pwd/reptools.pm";

my ($in,$o,$car,$from,$to,$step,$thrs,$v,$h);			

my $tmp = "$wd/tmp";

# scripts

my $toGeneratePacking = "$pwd/generate_packing.pl";

GetOptions
(
	"in=s" => \$in,							# csv input matrix
	"from=s" => \$from,						# inf bound threshold 'from' (included)
	"to=s" => \$to,							# sup bound threshold 'to' (included)
	"step=s" => \$step,						# step
	"thrs=s" => \$thrs,						# discrete threshold
	"car=s" => \$car,						# tab file
	"v" => \$v,								# verbose mode
	"o=s" => \$o,							# html output file
	"help" => \$h							# show help
);

if ($h)
{
	&help();
	exit;
}

unless ($in and $car and $o) 
{
	print("\nMissing arguments.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	print("See help with `perl ".basename($0)." --help` \n\n");
	exit;
}

if ( $o eq "tools" or $o eq "ext" or $o eq "representations" )
{
	print("\nValue for option '-o' '$o' forbidden.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	exit;
}

system "perl $toGeneratePacking -in=$in -car=$car -from=$from -to=$to -step=$step -o=circletemp";
system "cp circletemp/circletemp.html $o";
system "rm -rf circletemp";

exit;

=head2	function help

	Title			: help
	Usage			: &help()
	Prerequisite		: none
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." - Constructs a JSON hierarchy with a CSV matrix, a characteristics file and thresholds, returns a directory named  after the -o option and a HTML file named after -o option with the extension .html in the working directory\n"); 
	print("\n");
	print("\t-in=\t\t\t<csv file>\t\tThe input matrix\n");
	print("\t-car=\t\t\t<tab file>\t\tThe input characteristics file\n\n");
	print("\t-o=\t\t\t<string>\t\thtml output files\n\n");
	print("\t-from=\t\t\t<float>\t\t\tThe input lower bound for the threshold\n");
	print("\t-to=\t\t\t<float>\t\t\tThe input upper bound for the threshold\n");
	print ("\t-step=\t\t\t<float>\t\t\tThe step from the inf to the sup threshold\n");
	print("\t\tOR\n");
	print("\t-thrs=\t\t\t'<float>;<float>;...'\tA discrete input for the thresholds (order does not matter)\n");
	
	print("\n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n\n");
	print("\t--help\t\t\tDisplay help\n");
	print("\t-v \t\t\tVerbose mode\n");
	print("\n");

	print("Examples\n");
	
#	print("\t>\tperl wrapper_packing.pl -in=mat.csv -thrs='0.95;0.99' -o=output -car=groups.tab \n\n");
	print("\t>\tperl wrapper_packing_thrs.pl -in=mat.csv -from=0.95 -to=0.99 -step=0.01 -o=output -car=groups.tab \n\n");

}
