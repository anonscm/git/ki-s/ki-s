#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);

my $pwd = dirname(realpath($0));

require "$pwd/tools.pm";

my ($label_list,$inputname,$outputname,$h);

GetOptions
(
	"li=s" => \$label_list,
	"in=s" => \$inputname,
	"out=s" => \$outputname,
	"help"	=> \$h
);


if ( $h )
{
	&help();
	exit;
}

unless ($label_list and $inputname and $outputname)
{
	print("Missing arguments.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	exit;
}

&extract_mat ($label_list,$inputname,$outputname);


=head2	function help

	Title			: help
	Usage			: &help()
	Prerequisite	: none
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." - Extracts a matrix given a list of labels, an input file and an output file.\n"); 
	print("\n");
	print("\t-li=\t\t<txt file>\t\tThe file containing the list of labels to extract\n");
	print("\t-in=\t\t<csv file>\t\tThe input matrix\n");
	print("\t-out=\t\t<csv file>\t\tThe output matrix\n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n\n");
	print("\t--help\t\t\tDisplay this help\n");
	print("\n");

	print("Example\n");
	
	print("\t>\tperl extract_mat.pl -in=fasta6.csv -out=fasta3.csv -li=list.txt\n \n");
}


