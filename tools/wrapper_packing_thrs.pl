#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);


my $pwd = dirname(realpath($0));
my $wd = getcwd();	

require "$pwd/reptools.pm";

my ($in,$o,$car,$thr1,$thr2,$thr3,$v,$h);	

my $tmp = "$wd/tmp";

# scripts

my $toGeneratePacking = "$pwd/generate_packing.pl";

GetOptions
(
	"in=s" => \$in,							# csv input matrix
	"thr1=f" => \$thr1,						# threshold 1
	"thr2=f" => \$thr2,							# threshold 2
	"thr3=s" => \$thr3,						# threshold 3
	"car=s" => \$car,						# tab file
	"v" => \$v,								# verbose mode
	"o=s" => \$o,							# html output file
	"help" => \$h							# show help
);

if ($h)
{
	&help();
	exit;
}

unless ($in and $car and $o) 
{
	print("\nMissing arguments.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	print("See help with `perl ".basename($0)." --help` \n\n");
	exit;
}

if ( $o eq "tools" or $o eq "ext" or $o eq "representations" )
{
	print("\nValue for option '-o' '$o' forbidden.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	exit;
}

system "perl $toGeneratePacking -in=$in -car=$car -thrs='$thr1;$thr2;$thr3' -o=circletemp -v 2>&1";
system "cp circletemp/circletemp.html $o";
system "rm -rf circletemp";

exit;

=head2	function help

	Title			: help
	Usage			: &help()
	Prerequisite		: none
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." - Constructs a JSON hierarchy with a CSV matrix, a characteristics file and thresholds, returns a directory named  after the -o option and a HTML file named after -o option with the extension .html in the working directory\n"); 
	print("\n");
	print("\t-in=\t\t\t<csv file>\t\tThe input matrix\n");
	print("\t-car=\t\t\t<tab file>\t\tThe input characteristics file\n\n");
	print("\t-o=\t\t\t<string>\t\thtml output files\n\n");
	print("\t-thr1=\t\t\t<float>\t\t\tThe first threshold\n");
	print("\t-thr2=\t\t\t<float>\t\t\tThe second threshold\n");
	print ("\t-thr3=\t\t\t<float>\t\t\tThe third threshold\n");
	
	print("\n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n\n");
	print("\t--help\t\t\tDisplay help\n");
	print("\t-v \t\t\tVerbose mode\n");
	print("\n");

	print("Example\n");
	
	print("\t>\tperl wrapper_packing_thrs.pl -in=mat.csv -thr1=0.5 -thr2=0.8 -thr3=0.95 -o=output -car=groups.tab \n\n");
}
