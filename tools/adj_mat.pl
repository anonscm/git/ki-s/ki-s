#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);


my $pwd = dirname(realpath($0));
require "$pwd/tools.pm";


my ($in,$out,$t,$h,$v);			



GetOptions
(
	"in=s" => \$in,							# csv file input
	"out=s" => \$out,						# csv file output 
	"t=s" => \$t,							# a threshold
	"help" => \$h,							# show help
	"v"	=> \$v								# verbose
);


if ($h)
{
	&help();
	exit;
}

unless ($in and $out and $t)
{
	print("\nMissing arguments.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	print("See help with `perl ".basename($0)." --help` \n\n");
	exit;
}


if ($v)
{
	print("Creating adjacency matrix for '".basename($in)."' in file '".basename($out)."'. \nThe threshold is $t.\n");
}

&adjacency_mat($t,$in,$out);

if ($v)
{
	print("Done!");
	print("\n");
}
exit;




=head2	function help

	Title			: help
	Usage			: &help()
	Prerequisite		: none
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." - Converts a matrix from a file into an adjacency matrix, given a threshold.\n"); 
	print("\n");
	print("\t-in=\t\t\t<csv file>\t\t\tThe input matrix\n");
	print("\t-out=\t\t\t<csv file>\t\t\tThe output matrix\n");
	print("\n");
	print("\n");
	print("\t-t=\t\t\t<float>\t\t\t\tA threshold \n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n\n");
	print("\t--help\t\t\tDisplay help\n");
	print("\t-v \t\t\tVerbose mode\n");
	print("\n");

	print("Example\n");
	
	print("\t>\tperl adj_mat.pl -in=mat.csv -out=mat_adj.csv -t=0.95 -v\n\n");
	

}

