#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
#use List::MoreUtils qw(first_index);
#use List::MoreUtils qw{any};
use Storable qw(dclone);		# for raw copy 

=head1 NAME

    tools.pm - Module for matrices and files.
    Copyright (C) 2017  Mariam Bouzid

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


=head1 SYNOPSIS

	You can include this module with 'require "tools.pm"; '


=head1 DESCRIPTION

	Usefule module for matrix and file handling.
		

=head1 SUBROUTINES

	mkdir
	mv
	zip
	unzip
	remove_file
	remove_dir
	trim
	trim_file
	commas_to_dots
	get_mat
	change_separator
	from_tab_to_csv
	invert
	translate
	to_dist
	to_sym
	extract
	to_adjacency
	print_mat
	from_mat_to_graph
	sort_mat
	invert_mat
	extract_mat
	adjacency_mat
	percentage_mat
	dist_mat
	to_sym_mat
	listing

=cut

=head2 function mkdir

	Title			: mkdir
	Usage			: &mkdir ($dirname)
	Function		: Creates a directory given a name $dirname
	Returns			: none
	Args			: $dirname a scalar

=cut

sub mkdir
{
	my ($dirname) = @_;
	unless (-d $dirname)
	{
		`mkdir $dirname`;
	}
	else
	{
		print "$dirname already exists.\n Exiting. \n\n";	
	}
}

=head2 function mv

	Title			: mv
	Usage			: &mv ($path1,$path2)
	Function		: Move or rename $path1 to $path2
	Returns			: none
	Args			: $path1 a scalar ; $path2 a scalar

=cut

sub mv
{
	my ($path1,$path2) = @_;
	
	`mv $path1 $path2`;
	
}

=head2 function zip

	Title			: zip
	Usage			: &zip ($o,$z)
	Function		: zip the given input directory $o into the archive $z
	Returns			: none
	Args			: $o a scalar; $z a scalar	

=cut

sub zip
{
	my ($o,$z) = @_;
	if (-d $o)
	{
		`zip -j -qq -r $z $o 2>&1`
	}
	else
	{
		print STDERR "Error with $o. No such directory. \n Exiting.\n\n";
	}
}



=head2 function get_extension

	Title			: get_extension
	Usage			: my $ext = &get_extension($filename)
	Function		: Gets the format of the given filename
	Returns			: a scalar
	Args			: $filename a scalar 

=cut

sub get_extension
{
	my ($filename) = @_;
	my ($ext) = ($filename =~ m/.*\.(.*)$/);
	return ($ext);
}



=head2 function unzip

	Title			: unzip
	Usage			: &unzip($z,$o)
	Function		: Unzip the given zip file $z into the directory $o
	Returns			: none
	Args			: $z a scalar ; $o a scalar

=cut

sub unzip
{
	my ($z,$o) = @_;
	if (-f $z)
	{
		`unzip -qq -j $z -d $o 2>&1`
	}
	else
	{
		print STDERR "Error with $z. No such zip archive.\n \n";
	}
}

=head2 function remove_file 

	Title			: remove_file
	Usage			: &remove_file ($f)
	Function		: Removes a given file $f
	Returns			: none
	Args			: $f a scalar

=cut

sub remove_file
{
	my ($f) = @_;
	if (-f $f)
	{
		`rm $f`;
	}
	else
	{
		print STDERR "Error with $f. No such file.\n Exiting. \n\n";
	}
}

=head2 function remove_dir

	Title			: remove_dir
	Usage			: &remove_dir ($dirname)  
	Function		: Removes a given directory $dirname 
	Returns			: none 
	Args			: $dirname a scalar

=cut

sub remove_dir
{
	my ($dirname) = @_;
	if (-d $dirname)
	{
		`rm -rf $dirname`;
	}
	else
	{
		print STDERR "Error with $dirname. No such directory.\n Exiting.\n\n";
	}
}


=head2 function trim

	Title			: trim
	Usage			: &trim (\$str)
	Function		: Removes tab, br and spaces from a string $str
	Returns			: none
	Args			: $str a scalar

=cut

sub trim
{
	my ($rs_str) = shift;
	$$rs_str  =~ s/\s+|\n+|\t+//;
}

=head2 function trim_file

	Title			: trim_file
	Usage			: &trim_file ($f,$f_trimmed)
	Function		: Trims the file $f and puts the result in $f_trimmed
	Returns			: none
	Args			: $f a scalar; $f_trimmed a scalar
	
=cut

sub trim_file
{
	my ($f,$f_trimmed) = (@_);
	my $tmp = "tmp";
	`sed 's/ //g' $f > $tmp `;
	&mv ($tmp,$f_trimmed);

}


=head2 function commas_to_dots

	Title			: commas_to_dots
	Usage			: &commas_to_dots ($f_commas,$f_dots)
	Function		: Converts all the ',' into '.' from the file $f_commas into an other file $f_dots
	Returns			: none
	Args			: $f_commas a scalar; $f_dots a scalar
	
=cut

sub commas_to_dots
{
	my ($f_commas,$f_dots) = (@_);
	my $tmp = "tmp";
	`sed 's/,/./g' $f_commas > $tmp `;
	&mv ($tmp,$f_dots);

}

=head2 function change_separator

	Title			: change_separator
	Usage			: &change_separator ($csv,$tab,$sep1,$sep2)
	Function		: Converts a CSV file $csv with the separator $sep1 into a TAB file $tab with the separator $sep2
	Returns			: none
	Args			: $csv a scalar; $tab a scalar; $sep1 a scalar; $sep2 a scalar
	
=cut

sub change_separator
{
	my ($csv,$tab,$sep1,$sep2) = (@_);
	my $tmp = "tmp";
	`sed 's/$sep1/$sep2/g' $csv > $tmp `;
	&mv ($tmp,$tab);

}

=head2 function from_tab_to_csv

	Title			: from_tab_to_csv
	Usage			: &from_tab_to_csv ($tab,$csv)
	Function		: Converts a TAB file $tab into a CSV file $csv
	Returns			: none
	Args			: $tab a scalar, $csv a scalar
	
=cut

sub from_tab_to_csv
{
	my ($tab,$csv) = @_;
	my $tmp = "tmp";
	`sed 's/;/\\t/g' $tab > $tmp`;
	&mv($tmp, $csv);	

}

=head2 function get_mat

	Title			: get_mat
	Usage			: my ($ra,$rh) = &get_mat ($in)
	Function		: Gets a matrix representation from the given file $in, sorts the labels.
	Returns			: $ra a reference to an array ; $rh a hash reference 
	Args			: $in a scalar

=cut

sub get_mat
{
	my ($in) = @_;
	open (my $fh_input,$in);

	my $colnames = <$fh_input>;
	
	my (@a_labels) = split(';',$colnames);
	
	
	shift (@a_labels);

	my $n = scalar(@a_labels);

	&trim(\$a_labels[$n-1]);		


	my %h_matrix = ();

	while ( my $line = <$fh_input> )
	{
		my ($j,@a_coeffs) = split(';',$line);
		my $m = scalar(@a_coeffs);
		
		for (my $k = 0; $k < $m; ++$k)
		{
			$h_matrix{$j}{$a_labels[$k]} = $a_coeffs[$k];	
			&trim(\$h_matrix{$j}{$a_labels[$k]});
		}
	}
	
	close($fh_input);
	
	@a_labels = sort (@a_labels);

	return (\@a_labels,\%h_matrix);	
}

=head2 function to_adjacency

	Title			: to_adjacency
	Usage			: my ($ra,$rh) = &to_adjacency ($threshold,$ra,$rh)
	Function		: Converts the matrix represented by $ra and $rh into an adjacency matrix given a threshold $t
	Returns			: $ra a reference to an array; $rh a hash reference
	Args			: $threshold a scalar; $ra a reference of an array; $rh a reference of a hash array

=cut

sub to_adjacency
{
	my ($threshold,$ra,$rh) = (@_);

	for my $i (@{$ra})
	{
		for my $j (@{$ra})
		{
			if ( $rh->{$i}->{$j} >= $threshold )
			{
				$rh->{$i}->{$j} = 1;
			}
			else
			{
				$rh->{$i}->{$j} = 0;	
			};
		}
	}
	
	return ($ra,$rh);
}



=head2 function invert

	Title			: invert
	Usage			: my ($ra_inv,$rh_inv) = &invert($ra,$rh)
	Function		: Inverts the matrix represented by $ra and $rh
	Returns			: $ra_inv a reference to an array; $rh_inv a hash reference
	Args			: $ra a reference of an array; $rh a reference of a hash array

=cut

sub invert
{
	my ($ra,$rh) = @_;

	for my $i (@{$ra})
	{
		for my $j (@{$ra})
		{
			
			$rh->{$i}->{$j} = 1 - $rh->{$i}->{$j};
		}
	}
	
	return ($ra,$rh);
}


=head2 function to_dist

	Title			: to_dist
	Usage			: my ($ra_dist,$rh_dist) = &to_dist($ra,$rh)
	Function		: Computes the distance matrix represented by $ra and $rh
	Returns			: $ra_dist a reference to an array; $rh_dist a hash reference
	Args			: $ra a reference of an array; $rh a reference of a hash array

=cut

sub to_dist
{
	my ($ra,$rh) = @_;

	for my $i (@{$ra})
	{
		for my $j (@{$ra})
		{
			if ($rh->{$i}->{$j} != 0)
			{
				$rh->{$i}->{$j} = - log ( $rh->{$i}->{$j});
			}
			else
			{
				$rh->{$i}->{$j} = 1000;
			}
		}
	}
	
	return ($ra,$rh);
}

=head2 function translate

	Title			: translate
	Usage			: my ($ra_p,$rh_p) = &translate ($ra,$rh,$factor)
	Function		: Translates the matrix represented by $ra and $rh by a factor $factor
	Returns			: $ra_p a reference to an array; $rh_p a hash reference
	Args			: $ra a reference of an array; $rh a reference of a hash array; $factor a scalar

=cut

sub translate
{
	my ($ra,$rh,$factor) = @_;


	for my $i (@{$ra})
	{
		for my $j (@{$ra})
		{
			$rh->{$i}->{$j} = ( $rh->{$i}->{$j} ) * $factor;
		}
	}
	
	return ($ra,$rh);
}

=head2 function to_sym

	Title			: to_sym
	Usage			: my ($ra_sym,$rh_sym) = &to_sym($ra,$rh)
	Function		: Transforms the input asymetric matrix represented by $ra and $rh into a symetric matrix represented by $ra_sym
						and $rh_sym
	Returns			: $ra_sym a reference to an array; $rh_sym a hash reference
	Args			: $ra a reference of an array; $rh a reference of a hash array

=cut

sub to_sym
{
	my ($ra,$rh) = @_;
	my $n = scalar (@{$ra});

	for (my $i = 0; $i < $n; ++$i)
	{
		for (my $j = $i ; $j < $n; ++$j)
		{
			my $avg = ($rh->{$ra->[$i]}->{$ra->[$j]}+$rh->{$ra->[$j]}->{$ra->[$i]})/2 ;	
				
			$rh->{$ra->[$i]}->{$ra->[$j]} = $avg ;
			$rh->{$ra->[$j]}->{$ra->[$i]} = $avg ;
				
		}
	}
	return ($ra,$rh);
}


=head2 function extract

	Title			: extract
	Usage			: my ($ra_label_list) = &extract($listfile,$ra_labels)
	Function		: Transforms the element of the file $listfile into an array, checks if the array $ra_labels contains all the elements
	Returns			: $ra_label_list a reference to an array
	Args			: $listfile a scalar, $ra_labels a reference to an array

=cut

sub extract
{
	my ($listfile,$ra_labels) = @_;
	
	open (my $fh_input, $listfile);
	my @a_labels = ();
	
	
	while ( my $line = <$fh_input> )
	{
		&trim(\$line);
		push (@a_labels, $line);
	}
	
	@a_labels = sort (@a_labels);
	
	close ($fh_input);
	
	foreach my $element (@a_labels)
	{
		if (!( grep $_ eq $element, @$ra_labels))
		{
			print STDERR ("\nError with the list. No such element '$element' in the matrix .\nExiting. \n\n");
			exit;
		}
	}
	return (\@a_labels);
}


=head2 function display_mat
	
	Title			: display_mat
	Usage			: &display_mat ($ra,$rh)
	Function		: Displays the matrix represented by $ra and $rh in STDOUT
	Returns			: none
	Args			: $ra_labels a reference to an array; $rh_matrix a hash reference

=cut

sub display_mat
{
	my $ra_labels = shift;
	my $rh_matrix = shift;

	foreach my $i (@{$ra_labels})
	{
		print ";$i";
		
	}

	print "\n";
	
	foreach my $i (@{$ra_labels})
	{
		print $i;
		foreach my $j (@{$ra_labels})
		{
			printf ";".$rh_matrix->{$i}->{$j};
		}
		print "\n";
	
	}

}

=head2 function print_mat
	
	Title			: print_mat
	Usage			: &print_mat ($out,$ra_colnames,$rh_values)
	Function		: Prints the matrix represented by $ra_colnames and $rh_values into an output file $out
	Returns			: none
	Args			: $out a scalar; $ra_colnames a reference to an array; $rh_values a reference to a hash

=cut

sub print_mat
{
	my ($out,$ra_colnames,$rh_values) = @_;


	open (my $fh_output,">".$out);
	 
	foreach my $i (@{$ra_colnames})
	{
		print $fh_output ";$i";

	}

	print $fh_output "\n";
	
	foreach my $i (@{$ra_colnames})
	{
		print $fh_output $i;
		foreach my $j (@{$ra_colnames})
		{
			print $fh_output ";".$rh_values->{$i}->{$j};
		}
		print $fh_output "\n";

	}

	close($fh_output);
}

=head2 function from_mat_to_graph

	Title			: from_mat_to_graph
	Usage			: &from_mat_to_graph ($in,$out)
	Function		: Converts the file matrix $in into a graph output $out
	Returns			: none
	Args			: $in a scalar; $out a scalar

=cut

sub from_mat_to_graph
{
	my ($in,$out) = @_;

	my ($ra_colnames,$rh_values) = &get_mat($in);



	open (my $fh_output,">".$out);
	
	foreach my $i (@{$ra_colnames})
	{
		foreach my $j (@{$ra_colnames})
		{
			my $avg = ($rh_values->{$i}->{$j} + $rh_values->{$j}->{$i})/2;
			printf $fh_output "$i\t$j\t%.4f\n",$avg ; 
		}

	}
	
	close($fh_output);
	
}

=head2 function sort_mat

	Title			: sort_mat
	Usage			: &sort_mat ($in,$out)
	Function		: Sorts a matrix from the file $in and writes the modification into the output file $out
	Returns			: none
	Args			: $in a scalar; $out a scalar

=cut

sub sort_mat
{
	my ($in,$out) = @_;

	my ($ra_labels,$rh_matrix) = &get_mat($in);
	&print_mat ($out,$ra_labels,$rh_matrix);	
	
}

=head2 function invert_mat

	Title			: invert_mat
	Usage			: &invert_mat ($in,$out)
	Function		: Inverts the matrix from the file $in and writes the modification into the output file $out 
	Returns			: none
	Args			: $in a scalar; $out a scalar

=cut

sub invert_mat
{
	my ($in,$out) = @_;

	my ($ra_labels,$rh_matrix) = &get_mat($in);
	my ($ra_labels_inv,$rh_matrix_inv) = &invert ($ra_labels,$rh_matrix);
	&print_mat ($out,$ra_labels_inv,$rh_matrix_inv);	
}


=head2 function adjacency_mat

	Title			: adjacency_mat
	Usage			: &adjacency_mat ($t,$in,$out)
	Function		: Computes the adjacency matrix given a threshold $t from the file $in and writes the modification into the output file $out
	Returns			: none
	Args			: $t a scalar; $in a scalar; $out a scalar

=cut

sub adjacency_mat
{
	my ($t,$in,$out) = @_;

	my ($ra_labels,$rh_matrix) = &get_mat($in);
	my ($ra_labels_adj,$rh_matrix_adj) = &to_adjacency ($t,$ra_labels,$rh_matrix);
	&print_mat ($out,$ra_labels_adj,$rh_matrix_adj);	
	
}

=head2 function extract_mat

	Title			: extract_mat
	Usage			: &extract_mat ($from,$to,$in,$out)
	Function		: Extracts the matrix from the file $in from the label $from to the included label $to; puts the results into the output $out 
	Returns			: none
	Args			: $from a scalar; $to a scalar ; $in a scalar; $out a scalar

=cut

sub extract_mat
{
	my ($listfile,$in,$out) = @_;

	my ($ra_labels,$rh_matrix) = &get_mat($in);
	($ra_labels) = &extract ($listfile,$ra_labels);
	&print_mat ($out,$ra_labels,$rh_matrix);	
	
}


=head2 function translate_mat

	Title			: translate_mat
	Usage			: &translate_mat ($in,$out,$factor)
	Function		: Translates by a factor $factor the matrix from the file $in, puts it in $out 
	Returns			: none
	Args			: $in a scalar; $out a scalar; $factor a scalar

=cut

sub translate_mat
{
	my ($in,$out,$factor) = @_;

	my ($ra_labels,$rh_matrix) = &get_mat ($in);
	my ($ra_labels_perc,$rh_matrix_perc) = &translate ($ra_labels,$rh_matrix,$factor);
	&print_mat($out,$ra_labels_perc,$rh_matrix_perc);	
	
}

=head2 function to_sym_mat

	Title			: to_sym_mat
	Usage			: &to_sym_mat ($in,$out)
	Function		: Transforms the input asymetric matrix $in into a symetric matrix $out by computing the average between 
					  coefficient a_ij and a_ji
	Returns			: none
	Args			: $in a scalar; $out a scalar

=cut

sub to_sym_mat
{
	my ($in,$out) = @_;

	my ($ra_labels,$rh_matrix) = &get_mat ($in);
	my ($ra_labels_sym,$rh_matrix_sym) = &to_sym($ra_labels,$rh_matrix);
	&print_mat($out,$ra_labels_sym,$rh_matrix_sym);	
	
}

=head2 function dist_mat

	Title			: dist_mat
	Usage			: &dist_mat ($in,$out)
	Function		: Converts the matrix from the file $in into a distance matrix $out 
	Returns			: none
	Args			: $in a scalar; $out a scalar

=cut

sub dist_mat
{
	my ($in,$out) = @_;

	my ($ra_labels,$rh_matrix) = &get_mat($in);
	my ($ra_labels_perc,$rh_matrix_perc) = &to_dist($ra_labels,$rh_matrix);
	&print_mat($out,$ra_labels_perc,$rh_matrix_perc);	
}

=head2 function listing

	Title			: listing
	Usage			: &listing ($inputname,$outputname)
	Function		: List the files in $in and put it into $out
	Returns			: none
	Args			: $inputname a scalar; $outputname a scalar

=cut

sub listing
{
	my ($inputname,$listname) = @_;
	my @files = ();

	if ( -d $inputname )
	{
		(@files) = `ls -1 $inputname`;
	}
	elsif ( -f $inputname )
	{
		(@files) = `zipinfo -1 $inputname`;
	}
	else
	{
		print("$inputname is nor a directory neither a file.\n Exiting.\n\n");
		exit;
	}
	
	open (my $fh_output,">".$listname);
	foreach my $file (@files)
	{
		my ($id) = ( $file =~ m/^(.*)\..*$/);
		print $fh_output "$id\n";
	}
	close ($fh_output);	
}

=head2 function nb2dist

	Title			: nb2dist
	Usage			: my ($ra_inv,$rh_inv) = &nb2dist($ra,$rh)
	Function		: nb2dist the matrix represented by $ra and $rh
	Returns			: $ra_inv a reference to an array; $rh_inv a hash reference
	Args			: $ra a reference of an array; $rh a reference of a hash array

=cut

sub nb2dist
{
	my ($ra,$rh) = @_;

	for my $i (@{$ra})
	{
		for my $j (@{$ra})
		{
			if ($rh->{$i}->{$j} > 0)
      {
			  $rh->{$i}->{$j} = 1 - (1/$rh->{$i}->{$j});
      }
      else
      {
        $rh->{$i}->{$j} = 0;
      }
		}
	}
	return ($ra,$rh);
}

=head2 function nb2dist_mat

	Title			: nb2dist_mat
	Usage			: &nb2dist_mat ($in,$out)
	Function		: nb2dist the matrix from the file $in and writes the modification into the output file $out 
	Returns			: none
	Args			: $in a scalar; $out a scalar

=cut

sub nb2dist_mat
{
	my ($in,$out) = @_;

	my ($ra_labels,$rh_matrix) = &get_mat($in);
  print length($ra_labels)."\n";
	my ($ra_labels_inv,$rh_matrix_inv) = &nb2dist ($ra_labels,$rh_matrix);
	&print_mat ($out,$ra_labels_inv,$rh_matrix_inv);	
}

return 1;
exit;

