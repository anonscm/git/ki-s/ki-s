#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);

my $pwd = dirname(realpath($0));
require "$pwd/tools.pm";

my ($inputname,$outputname,$h);

GetOptions
(
	"in=s" => \$inputname,
	"out=s" => \$outputname,
	"help" => \$h,
);

if ($h)
{
	&help();	
	exit;
}

unless ($inputname and $outputname)
{
	print("Missing arguments.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	exit;
}

&dist_mat($inputname,$outputname);
exit;



=head2	function help

	Title			: help
	Usage			: &help()
	Prerequisite		: none
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." - Converts a matrix from a file into a distance matrix by applying log for each a_ij.\n"); 
	print("\n");
	print("\t-in=\t\t\t<csv file>\t\t\tThe input matrix\n");
	print("\t-out=\t\t\t<csv file>\t\t\tThe output distance matrix\n");
	print("\n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n\n");
	print("\t--help\t\t\tDisplay help\n");
	print("\n");

	print("Example\n");
	
	print("\t>\tperl to_dist.pl -in=mat.csv -out=mat_dist.csv\n\n");
	

}

