#!/usr/bin/perl -w
#
use strict;
use warnings;
use Getopt::Long;
use File::Basename;
#use List::MoreUtils qw(first_index);
use List::MoreUtils qw{any};
use Storable qw(dclone);	
use Cwd qw(abs_path realpath getcwd);

my $pwd = dirname(realpath($0));
require "$pwd/tools.pm";

=head1 NAME

    reptools.pm - Module for representations
    Copyright (C) 2017  Mariam Bouzid

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


=head1 SYNOPSIS

	You can include this module with 'require "reptools.pm"; '


=head1 DESCRIPTION

	Usefule module for representation
		

=head1 SUBROUTINES

	get_groups
	merge_files
	get_colors
	create_flat_json

=cut

=head2 function get_groups

	Title			: get_groups
	Usage			: my ($rh_h, $rh_g, $ra_labels) = &get_groups ($in)
	Function		: Gets the group from the file $in
	Returns			: $rh_h a hash ref; $rh_g a hash ref; $ra_labels an array
	Args			: $in a scalar

=cut
sub get_groups
{
	my ($in) = @_;
	
	open (my $fh_input, $in);
	
	my (@a_columns) = split ("\t", <$fh_input>);
	shift (@a_columns);
	
	foreach my $col (@a_columns)
	{
		chomp ($col);
	}
	
	my %h_hierarchy ;
	my %h_groups ;
	my @a_elements;
	
	while ( my $line = <$fh_input> )
	{
		my @a_line = split("\t",$line);
		pop(@a_line);
		
		my $element = $a_line[0];
		
		push (@a_elements, $element);

		chomp ($element);
	
		
		my $m = scalar (@a_line);
		my @a_group_hierarchy = splice (@a_line,1,$m);
		
		
		
		my $n = scalar (@a_group_hierarchy);
		
		for (my $i = 0; $i < $n-1; ++$i)
		{
			print ($a_group_hierarchy[$i]."\n");
			chomp($a_group_hierarchy[$i]);
		}

		


		for (my $i = 0; $i < $n-1; ++$i)
		{
		
			if (! any { $_ eq $a_group_hierarchy[$i+1]} @{ $h_hierarchy{$a_group_hierarchy[$i]}}) 
			{
				push (@{ $h_hierarchy{ $a_group_hierarchy[$i] } }, $a_group_hierarchy[$i+1]); 
			}
			
		}
		push (@{ $h_groups{$a_group_hierarchy[$n-1]} },$element);
		
	}	
	
	close ($fh_input);
	
	
	return (\%h_hierarchy, \%h_groups, \@a_elements) ;
}


=head2 function merge_files

	Title			: merge_files
	Usage			: &merge_files ($inputlist,$output)
	Function		: Merges all the files given in the file $inputlist and puts the results in $output
	Returns			: none
	Args			: $inputlist a scalar, $output a scalar

=cut

sub merge_files
{
	my ($inputlist,$output) = @_;
	
	open (my $fh_input, $inputlist);
	my @a_files = ();
	
	
	while ( my $line = <$fh_input> )
	{
		&trim(\$line);
		push (@a_files, $line);
	}
	
	@a_files = sort (@a_files);
	
	close ($fh_input);

	
	my %h_group ;

	foreach my $file (@a_files)
	{
		open (my $fh_input, $file);
		my $header = <$fh_input> ;

		while ( my $line = <$fh_input> )
		{	
			my ($element, $group) = split ("\t",$line);
			&trim (\$element);
			&trim (\$group);			
			push (@{ $h_group{$element} },$group);
		}

		close ($fh_input);
	}
	

	open (my $fh_output, ">".$output);
	foreach my $filename (@a_files)
	{
		my ($name) = ($filename =~ m/(.*)\..*$/);
		print $fh_output "\t$name";
	}
	print $fh_output "\n";
	foreach my $key (keys %h_group)
	{
		my (@a_tmp) = @{ $h_group{$key} };
		print $fh_output $key."\t" ;
		foreach my $element (@a_tmp)
		{
			print $fh_output $element."\t";
		}
		print $fh_output "\n";
	}
	
	close ($fh_output);	
}

=head2 function get_colors

	Title			: get_colors
	Usage			: my ($rh) = &get_colors($in)
	Function		: Gets the colors in the input file $in and puts in a hash 
	Returns			: $rh a hash ref
	Args			: $in a scalar

=cut

sub get_colors
{
	my ($in) = @_;

	
	open (my $fh_input, $in);
	
	my (@a_columns) = split ("\t", <$fh_input>);
	shift (@a_columns);
	
	foreach my $col (@a_columns)
	{
		chomp ($col);
	}
	
	my %h_colors ;
	
	while ( my $line = <$fh_input> )
	{
		my ($element,$color) = split("\t",$line);
		chomp ($color);
		$h_colors{$element} = $color;
	}
	

	return (\%h_colors);
}

=head2 function create_flat_json

	Title			: create_flat_json
	Usage			: &create_flat_json ($out, $rh_hierarchy, $rh_groups, $rh_colors)
	Function		: Creates a flat JSON file with colors $rh_colors and given a group represented by $rh_hierarchy and $rh_groups
	Returns			: none
	Args			: $out a scalar; $rh_hierarchy a hash reference; $rh_groups a hash reference; $rh_colors a colors

=cut

sub create_flat_json
{
	my ($out, $rh_hierarchy, $rh_groups, $rh_colors) = @_;


	my %h_hierarchy = %{ dclone($rh_hierarchy)};
	my %h_groups = %{ dclone($rh_groups)};
	my %h_colors = %{ dclone($rh_colors)};

	my @a_child = ();
	foreach my $key (keys %h_hierarchy)
	{
		foreach my $children (@{ $h_hierarchy{$key} })
		{
			push (@a_child, $children);
		}
	}

	my @a_lines = ();

	
	foreach my $key (keys %h_hierarchy)
	{
		if ( ! any { $_ eq $key } @a_child )
		{
			push (@a_lines,"{\"name\":\"\", \"color\":0, \"parent\":\"\", \"id\":\"$key\",\"children\":[]}");	
		}
	
		if ( @{ $h_hierarchy{$key}} )
		{
			foreach my $children (@{ $h_hierarchy{$key} })
			{
				my $str = "{\"name\":\"\", \"color\":0, \"parent\":\"$key\", \"id\":\"$children\",";
				$str .="\"children\":[";
				my @a_sublines = ();
				foreach my $element (@{$h_groups{$children}})
				{
					push (@a_sublines,"{\"name\":\"$element\", \"size\":\"5\", \"color\":".$h_colors{$element}."}");	
				}
				
				my $sz_sublines = scalar(@a_sublines);
				my $counter = 0;
				foreach my $subline (@a_sublines)
				{
					$str .= $subline ;
					if ( ++$counter < $sz_sublines)
					{
						$str .= ",";
					}
				}
				
				$str .= "]}";
				push (@a_lines, $str);
			}
		}
	}
	
	
	open (my $fh_output, ">".$out);
	my $sz = scalar (@a_lines);
	my $counter = 0;
	print $fh_output "[";
	foreach my $line (@a_lines)
	{
		print $fh_output $line ;
		if ( ++$counter < $sz )
		{
			print $fh_output ",\n";
		}
	}
	print $fh_output "]\n";
	
	close ($fh_output);
}

return 1;
exit;


