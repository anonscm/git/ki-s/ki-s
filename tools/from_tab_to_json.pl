#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);


my $pwd = dirname(realpath($0));
my $wd = getcwd();	

require "$pwd/reptools.pm";


my ($in,$out,$h);			

GetOptions
(
	"in=s" => \$in,							# tab input file
	"out=s" => \$out,						# json output file
	"help" => \$h							# show help
);


if ($h)
{
	&help();
	exit;
}

unless ($in and $out) 
{
	print("\nMissing arguments.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	print("See help with `perl ".basename($0)." --help` \n\n");
	exit;
}


my ($rh_colors) = &get_colors ($in);

my @a_lines = ();

foreach my $key (keys %{$rh_colors})
{
	my $color = $rh_colors->{$key};
	push (@a_lines,"{ \"color\":$color, \"legend\":\"$key\" }");
	
}

my $sz = scalar (@a_lines);
my $n = $sz -1 ;
open (my $fh_output, ">$out");
print $fh_output "[\n";
for (my $i=0; $i < $n; ++$i)
{
	print $fh_output $a_lines[$i].",";
}

print $fh_output $a_lines[$n] ;

print $fh_output "\n]\n";
close($fh_output);


exit;

=head2	function help

	Title			: help
	Usage			: &help()
	Prerequisite		: none
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." - Converts a TAB file into a JSON file\n"); 
	print("\n");
	print("\t-in=\t\t\t<tab file>\t\t\tThe input groups\n");
	print("\t-out=\t\t\t<json file>\t\t\tThe output groups\n");
	print("\n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n\n");
	print("\t--help\t\t\tDisplay help\n");
	print("\t-v \t\t\tVerbose mode\n");
	print("\n");

	print("Example\n");
	
	print("\t>\tperl from_tab_to_json.pl -in=grps.tab -out=grps.json\n\n");
	

}

