#!/usr/bin/env Rscript
args <- commandArgs (trailingOnly = TRUE)

######################################################################
# 
# + -----------------------------------------------------------------+
# |																		
# |  composante_connexe.r : regroupement à partir d'une matrice
# |		d'adjacence
# |
# |  de Marc Legeay
# |																	 
# +------------------------------------------------------------------+
#
#######################################################################
nomscript <- "composante_connexe.r"
#######################################################################



showHelp <- function (nom)
{
	cat("\n\n")
	cat (nom," - Regroupements à partir d'une matrice d'adjacence.\n")
	cat ("\tde Marc Legeay\n")
	cat ("\n")
	cat ("Aucune dépendance. \n")
	cat ("\n")
	cat ("Utilisation: \n")
	cat ("> Rscript ",nom," <matrice d'adjacence [CSV]> <fichier de groupes [TAB]> \n")
	cat("\n\n")
	quit();
}


fichierMatriceCSV <- args[1]
fichierGroupesTAB <- args[2]
  
n <- length (args)

for (i in (1:n))
{
	if ( args[i] == 'help' || args[i] == 'h' )
	{

		showHelp (nomscript);
	}
}

# graphe : matrice d'adjacence (0 si non-voisin, !=0 sinon)
composantes <- function (matriceCSV)
{
  
  graphe <- as.matrix(read.csv(matriceCSV, sep=';', row.names = 1))
  
  noeuds <- rep(TRUE, nrow(graphe))
  
  composantes <- NULL
  while(TRUE %in% noeuds) { # Tant qu'il reste des noeuds
  	composante <- NULL
    a_visiter <- which(noeuds)[1] # On prend le premier noeud non-visité
    while(length(a_visiter)>0) { # Tant qu'il reste des noeuds à visiter dans la composante ...
      n <- a_visiter[1] # On prend le premier
      # On le marque comme visité
      a_visiter <- a_visiter[-1]
      noeuds[n] <- FALSE
      
      composante <- c(composante, rownames(graphe)[n])
      
      # On rajoute ses voisins pour les regarder plus tard
      a_visiter <- union(a_visiter, which(((graphe[n,]!=0)|(graphe[,n]!=0))&noeuds)) # En enlevant ceux qu'on a déjà vu
    }
    composantes <- c(composantes, list(composante))
  }
  
  return(composantes)
}


lesComposantes <- composantes(fichierMatriceCSV)

nomMatrice <-  sub ("\\.(.*)", '', basename(fichierMatriceCSV) )

nomcluster <- paste("\t", nomMatrice , sep='' )

write(nomcluster,fichierGroupesTAB,append=FALSE)

i <- 1
for (groupe in lesComposantes)
{
	for (membre in groupe)
	{
		row <- paste(membre,"\tG_",i,"_",nomMatrice,sep='')
		write(row,fichierGroupesTAB,append=TRUE)
	}
	i <- i+1
}



