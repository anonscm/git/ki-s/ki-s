#!/usr/bin/perl -w
#

use strict;
my $pwd = dirname(realpath($0));			# path to working directory
my $version = '1.2';      						# version


require "$pwd/tools/tools.pm";

use Carp;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);
use threads ('yield','stack_size' => 64*4096, 'exit' => 'threads-only', 'stringify');

=head1 NAME

    ki-s.pl - Compute ANI or shared k-mers given a zip file.
    Copyright (C) 2017  Mariam Bouzid, Martial Briand
                           
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


=head1 SYNOPSIS

	ki-s.pl 

	-z= <zipfile> The input zip file
		OR
	-d= <directory> The input directory 

	-m= <ani|kmers|both> The specified method: ani or kmers or both
	
	-o= <output> The prefix output files name


	ADVANCED OPTIONS

	-t= <nbthreads> The number of threads to run [DEFAULT '4']
	-bl= <ANIb|ANIm> The blast version (legacy or blastn) [DEFAULT 'all']
	-k= <kmersize> The k-mer size [DEFAULT '22']
	-pk=<simka|kmerdb> The k-mer counting program [DEFAULT 'simka']\n

	-v <verbose mode> Display pyani and/or simka output

	
	DOCUMENTATION & HELP

	--help Display help
	--doc Display perldoc
	--version The current version

	Examples
	
	>	perl ANI_KMERS.pl -o=res2 -z=examples/fasta-2.zip -m=ani -v

	>	perl ANI_KMERS.pl -o=res3 -d=examples/fasta-3 -m=kmers


=head1 DESCRIPTION

	Given a zip or a directory containing fasta files (nucleic sequences), 
	this script returns similarity matrix of ANI and/or shared k-mer method.


=head1 SUBROUTINES

	simka_listing
	launch_simka
	compute_kmers
	launch_pyani
	compute_ani
	progression_ani
	preamble
	help

=cut

my ($z,$d,$m,$o,$t,$bl,$k,$pk,$v,$h,$doc,$vers);			
my @tmpc = ("A".."Z","a".."z",0..9);
&trim(\$pwd);
	
my $wd = getcwd();					# working directory


# SET BIN and SCRIPT path

my $bin_pyani = "$pwd/ext/pyani/bin/average_nucleotide_identity.py";	
my $bin_simka = "$pwd/ext/simka/bin/simka";
my $bin_kmerdb = "$pwd/ext/kmer-db/kmer-db-1.7.5";

# END SET

GetOptions
(
	"z=s" => \$z,							# zip file input 
	"d=s" => \$d,							# files directory 
	"m=s" => \$m,							# method
	"o=s" => \$o,							# output prefix name
	"t=s" => \$t,							# number of threads
	"bl=s" => \$bl,							# blast version
	"k=s" => \$k,							# k-mer size
  "pk=s" => \$pk,							# k-mer count program
	"v"=> \$v,								# verbose mode
	"help" => \$h,							# show help
	"doc" => \$doc,							# show perldoc
	"version" => \$vers						# show version
);

die `pod2text $0` if $doc ;

if ($h)
{
	&help();
	exit;
}

if ($vers)
{
	print("\n".basename($0)." version $version\n\n");
	unless ( $m and $o and $z or $d )
	{
		exit;
	}
}

unless ($m and $o and $z or $d)
{
	print("\nMissing arguments.. ¯\\_(ツ)_/¯ \nExiting.\n\n");
	print("See help with `perl ".basename($0)." --help` \n\n");
	exit;
}


&preamble();


# SET DEFAULT VALUES 

my $n;						# number of files

unless ( $t )
{
	$t = 4;
}

unless ( $bl )
{
	$bl = "all";
}

unless ( $k )
{
	$k = 22;
}

unless ( $pk )
{
	$pk = "simka";
}


# END SET


# TEST INPUT


if ( $z and $d )
{
	print("\nYou cannot give a zip AND a directory input.. ¯\\_(ツ)_/¯ \n Exiting.  \n\n");
	exit;
}

if ( $z )
{
	$z = &abs_path($z);
	$n = `zipinfo -1 $z | wc -l`;
#	($d) = ($z =~ m/^(.*)\..*$/);
#	$d = $wd."/".basename($d);
	$d = $wd."/".join("",@tmpc[map{rand @tmpc}(1..8)]);
}
elsif ( $d )
{
	$d = &abs_path($d);
	$n = `ls $d | wc -l`;
}


print ("Number of files: $n\n");
my $sz_mat = (($n*$n)*2)-$n*2;					# number of coefficient in the matrix

unless ( -d $d )
{
	
	print("Unzipping '".basename($z)."' ... ");
	&unzip($z,$d);
	print("successful!\n\n");
}

# END TEST	


# K-MER routine

if ( $m eq "kmers" or $m eq "both")
{
  print $pk."\n";
  # SET OUTPUT

  my $mat = "$wd/$o\_matrix\_kmers.csv";
  my $dist = "$wd/$o\_dist\_matrix\_kmers.csv";
  my $dendropdf = "$wd/$o\_dendro\_kmers.pdf";
  my $dendronwk = "$wd/$o\_dendro\_kmers.nwk";
  my $zip = "$wd/$o\_archive\_kmers.zip";

  # END SET

  # Count k-mers with simka
  if ( $pk eq "simka" )
  {
	  # SET SIMKA VAR
	  my $simka_results = $wd."/".join("",@tmpc[map{rand @tmpc}(1..8)]);
    #	my $simka_results = "$wd/simka_results";

	  if ( -d $simka_results )
	  {
		  &remove_dir ($simka_results);
	  }

	  my $mat_kmers = "$simka_results/mat_presenceAbsence_simka-jaccard_asym.csv";
	
	  # END SET

	  print("Computing kmers ...");
	  &compute_kmers($d,$bin_simka,$k,$simka_results,$v);
	  print("successful!\n\n");

#	  &mv($mat_kmers,$mat);
	
	  &invert_mat($mat_kmers,$mat);

#	  &dist_mat($mat,$dist);
	
	  `Rscript $pwd/representations/dendro.r $mat $dendropdf $dendronwk`;
	
#	  &mv ($dist,$simka_results);
	
	  &zip($simka_results,$zip);
	  `zip -j $zip $mat`;
	  `zip -j $zip $dendropdf`;
	  `zip -j $zip $dendronwk`;
	  &remove_dir ($simka_results);
  }
  # Count k-mers with kmer-db
  else
  {
    # SET KMERDB OUTPUT PREFIX
	  my $kmerdb_tmp_prefix = $wd."/".join("",@tmpc[map{rand @tmpc}(1..8)]);
    my $kmerdbOutput = $kmerdb_tmp_prefix.".mat";
	  # END SET

	  print("Computing kmers ...");
	  &count_kmers_with_kmerdb($d,$bin_kmerdb,$k,$kmerdb_tmp_prefix);
	  print("successful!\n\n");

    &mv($kmerdbOutput,$mat);

    `Rscript $pwd/representations/dendro.r $mat $dendropdf $dendronwk`;

    `zip -j $zip $mat`;
    `zip -j $zip $dendropdf`;
    `zip -j $zip $dendronwk`;
  }

	print("The results are in ". basename($mat).", ".basename($dendropdf).", ".basename($dendronwk)." and ".basename($zip)."\n\n");
}

# ANI routine

if ( $m eq "ani" or $m eq "both" )
{
	
	# SET PYANI VAR
	my $pyani_results = $wd."/".join("",@tmpc[map{rand @tmpc}(1..8)]);

#	my $pyani_results = "$wd/pyani_results";
	my $mat_ani = "$pyani_results/ANIb_percentage_identity.tab";
	my $graph_ani = "$pyani_results/ANIb_percentage_identity.pdf";
	
	if ( -d $pyani_results )
	{
		&remove_dir($pyani_results);
	}

	if ( $bl eq 'ANIm' )
	{
		$mat_ani = "$pyani_results/ANIm_percentage_identity.tab";
		$graph_ani = "$pyani_results/ANIm_percentage_identity.pdf";
	}

	# END SET

	print("Computing ANI ... \n");
	my $thr;
	
	if ( $v )
	{
		$thr = threads->create('progression_ani',$sz_mat,$bl,$pyani_results);
	}

	&compute_ani($d,$t,$bin_pyani,$bl,$pyani_results);
	
	print("done!\n\n");

	if ( $v )
	{
		$thr->join();
	}

	
	# SET OUTPUT

	my $mat = "$wd/$o\_matrix\_ani.csv";
	my $dist = "$wd/$o\_dist\_matrix\_ani.csv";
	my $graph = "$wd/$o\_graph\_ani.pdf";
	my $dendro = "$wd/$o\_dendro\_ani.pdf";
	my $zip = $wd."/$o\_archive\_ani.zip";
	
	&mv($mat_ani,$mat);
	&mv($graph_ani,$graph);
	
	&change_separator($mat,$mat,"\\t",";");
	&sort_mat($mat,$mat);
	
	&dist_mat($mat,$dist);
	
	`Rscript $pwd/representations/phylogenetic_tree.r $dist $dendro`;
	
	
	&mv ($dist,$pyani_results);

	&zip($pyani_results,$zip);
	&remove_dir($pyani_results);
	
#	&remove_file("formatdb.log");

	# END SET


	print("The results are in ". basename($mat).", ".basename($dendro).", ".basename($graph)." and ".basename($zip)."\n\n");
}



if ( $z )
{
	print("Removing directory ".basename($d)."...");
	&remove_dir($d);
	print("done!\n\n");
}



print(basename($0)." done.\n\n");
exit;

=head2 function simka_listing

	Title			: simka_listing
	Usage			: (@a_file_list) = &simka_listing ($d,$list)
	Function		: List all the fasta files of the directory $d in the file $list
	Returns			: an array
	Args			: $d a scalar; $list a scalar

=cut

sub simka_listing
{
	my ($d,$list) 		= @_;
	my @a_file_list = `ls -1 $d`;


	
	my $output = "$d/$list";
	open(my $fh_output,">".$output);

	foreach my $filename (@a_file_list)
	{
		my ($id) = ( $filename =~ m/^(.*)\.f.*$/);
		print $fh_output $id.": ".$filename;
	}
	
	close($fh_output);

	return (@a_file_list);
}



=head2 function launch_simka

	Title			: launch_simka
	Usage			: &launch_simka($d,$list,$k,$abmin,$outdir)
	Function		: Launch simka with the command $bin in the working directory $wd for the files listed in the file $list in $d, with parameters $k for the k-mer size and $abmin the abundance minimum with the verbose mode $v
	Returns			: none
	Args			: $d a scalar; $list a scalar; $k a scalar; $abmin a scalar; $bin a scalar; $wd a scalar; $v a scalar

=cut

sub launch_simka
{
	my ($d,$list,$k,$abmin,$bin,$simka_results,$v) = @_;
	
	my $tmp = $simka_results."_tmp";
	my $mem = 5000;
	my $mg = 128;
	my $nbcores = `nproc`;
	my $red = "";
	
	&trim(\$nbcores);
	
	
	if ( ! $v )
	{	
		$red = "2>&1";
	}

	print("Launching simka...\n");
	
	`$bin -in $d/$list -kmer-size $k -max-merge $mg -max-memory $mem -abundance-min $abmin -out $simka_results -out-tmp $tmp -nb-cores $nbcores $red` ;
	
	print ("done !\n");
	&remove_dir ($tmp);
	&remove_file("$d/$list");
}



=head2 function compute_kmers

	Title			: compute_kmers
	Usage			: &compute_kmers ($d,$bin,$k,$wd,$v)
	Function		: Compute with the command $bin in the working directory $wd all the shared $k-mers of the files in the directory $d with the mode verbose $v
	Returns			: none
	Args			: $d a scalar; $bin a scalar; $k a scalar; $wd a scalar; $v a scalar

=cut

sub compute_kmers
{
	my ($d,$bin,$k,$simka_results,$v) = @_;
	my $list = "list.txt";

	my $abmin = 1;

	unless (-f $d."/".$list )
	{
		print("Listing files for simka...");
		&simka_listing ($d,$list);
		print("done !\n");
	}	
	
	&launch_simka($d,$list,$k,$abmin,$bin,$simka_results,$v);
}



=head2 function kmerdb_listing

	Title			: kmerdb_listing
	Usage			: &kmerdb_listing ($d,$list)
	Function		: List all the fasta/fna/fa files of the directory $d in the file $list
	Returns			: an array
	Args			: $d a scalar; $list a scalar

=cut

sub kmerdb_listing
{
	my ($d,$list) 		= @_;
	my @a_file_list = `ls -1 $d`;
	
	my $output = "$list";
	open(my $fh_output,">".$output);

	foreach my $filename (@a_file_list)
	{
		my ($id) = ( $filename =~ m/^(.*)\.f.*$/ );
		print $fh_output $d."/".$id."\n";
	}
	
	close($fh_output);
}



=head2 function count_kmers_with_kmerdb

	Title			: count_kmers_with_kmerdb
	Usage			: &count_kmers_with_kmerdb($d,$bin,$k,$wd)
	Function		: Compute with the command $bin (kmer-db) in the working directory $wd all the shared $k-mers of the files in the directory $d
	Returns			: none
	Args			: $d a scalar; $bin a scalar; $k a scalar; $wd a scalar

=cut

sub count_kmers_with_kmerdb
{
	my ($d,$bin,$k,$kmerdb_tmp_prefix) = @_;
  my ($help,$kmerdbOutput,$kmerLength,$tmpList,$tmpDb,$tmpCount,$tmpMatrix,$ligne,$tabLength,$headerLine);
  my (@a_header);

  $tmpList = $kmerdb_tmp_prefix.".list";
  $tmpDb = $kmerdb_tmp_prefix.".db";
  $tmpCount = $kmerdb_tmp_prefix.".count";
  $tmpMatrix = $tmpCount.".min";
  $kmerdbOutput = $kmerdb_tmp_prefix.".mat";

	unless (-f $tmpList)
	{
		print("Listing files for kmer-db...");
		&kmerdb_listing ($d,$tmpList);
		print("done !\n");
	}
  # Launch kmer-db
	system "kmer-db build -k $k $tmpList $tmpDb";
  system "kmer-db all2all $tmpDb $tmpCount";
  system "kmer-db distance min $tmpCount";

  # Read kmer-db output, create sim matrix
  open (TMPMATFILE,$tmpMatrix);
  open (SIMMATFILE,">$kmerdbOutput");
  while ($ligne = <TMPMATFILE>)
  {
    chomp($ligne);
    $ligne =~ s/,/;/g;
    if ($ligne =~ /^kmer\-length\:[^\;]*(\;.*)\;/)
    {
      $headerLine = $1;
      print SIMMATFILE $headerLine."\n";
      @a_header = split(";",$headerLine);
      $tabLength = scalar(@a_header);
    }
    else
    {
      if ($ligne =~ /(.*)\;/)
      {
        print SIMMATFILE $1;
      }
      print SIMMATFILE ";1";
      $tabLength = $tabLength - 1;
      for (my $i=1;$i<$tabLength;$i++)
      {
        print SIMMATFILE ";NA";
      }
      print SIMMATFILE "\n";
    }
  }
  close (TMPMATFILE);
  close (SIMMATFILE);
  &remove_file($tmpList);
  &remove_file($tmpDb);
  &remove_file($tmpCount);
  &remove_file($tmpMatrix);
}



=head2 function launch_pyani

	Title			: launch_pyani
	Usage			: &launch_pyani ($d,$t,$bin,$bl,$wd)
	Function		: Lauch pyani with the command $bin in the working directory $wd for the files in the directory $d, using $t threads and using the blast version $bl
	Returns			: none
	Args			: $d a scalar; $t a scalar; $bin a scalar; $bl a scalar; $wd a scalar 

=cut

sub launch_pyani
{
	my ($d,$t,$bin,$bl,$pyani_results) = @_;
	$bin =~ s/\/$// ;
	my $m = 'ANIb';
	
	if ( $bl eq 'ANIm' )
	{
		$m = 'ANIm';
	}
	
	`$bin -i $d -o $pyani_results -g --gformat pdf --gmethod seaborn --workers $t -m $m 2> /dev/null`;
	
}

=head2 function compute_ani

	Title			: compute_ani
	Usage			: &compute_ani ($d,$t,$bin,$bl,$wd)
	Function		: Compute ANI in the working directory $wd given a directory $d containing the files, using the blast version $bl in $bin and using $t threads
	Returns			: none
	Args			: $d a scalar; $t a scalar; $bin a scalar; $bl a scalar; $wd a scalar

=cut

sub compute_ani
{
	my ($d,$t,$bin,$bl,$pyani_results) = @_;
	
	&launch_pyani($d,$t,$bin,$bl,$pyani_results);
}


=head2 function progression_ani

	Title			: progression_ani
	Usage			: &progression_ani($n,$bl,$wd)
	Function		: Display the progress for the computation of ANI for $n files using blast version $bl in the working directory $wd
	Returns			: none
	Args			: $n a scalar; $bl a scalar; $wd a scalar

=cut

sub progression_ani
{
	my ($n,$bl,$pyani_results) = @_;
	print("\nTotal: $n\n");
	my $count = 0;
	

	my $dir = "$pyani_results/blast$bl\_output/";


	while ( !( -d $dir) )
	{}

	while  ($count != $n)
	{
		
		my $old = $count ;
		if ( $count == $n)
		{
			last;
		}
		while ( ($old == $count) )
		{
			$count = `(ls -1 $dir | grep 'vs' | wc -l) 2> /dev/null`;
		}
		&trim(\$count);
		print ("\r$count/$n");
	
	}
	print("\n");
	
}


=head2	function preamble

	Title			: preamble
	Usage			: &preamble()
	Function		: Displays the GNU GPL Licence  
	Returns			: none
	Args			: none


=cut

sub preamble
{
	print("\n");
	print(basename($0)." version $version - Copyright (C) 2017  Mariam Bouzid, Martial Briand\n");
	print("This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'\n");
	print("This is free software, and you are welcome to redistribute it\n");
	print("under certain conditions; type `show c' for details.\n");
	print("\n");	

}

=head2	function help

	Title			: help
	Usage			: &help()
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." version ".$version."\n"); 
	print("\n");
	print("\t-z=\t\t\t<zipfile>\t\t\tThe input zip file\n");
	print("\t\tOR\n");
	print("\t-d=\t\t\t<directory>\t\t\tThe input directory\n");
	print("\n");
	print("\n");
	print("\t-m=\t\t\t<ani|kmers|both>\t\tThe specified method: ani or kmers or both\n");
	print("\t-o=\t\t\t<output>\t\t\tThe prefix output files name\n");
	print("\n");
	print("\n");
	print("ADVANCED OPTIONS\n");
	print("\n");
	print("\t-t=\t\t\t<nbthreads>\t\t\tThe number of threads to run [DEFAULT 4]\n");
	print("\t-bl=\t\t\t<ANIb|ANIm>\t\t\tANIb (blast+) or ANIm [DEFAULT ANIb]\n");
	print("\t-k=\t\t\t<kmersize>\t\t\tThe k-mer size [DEFAULT 22]\n");
	print("\t-pk=\t\t\t<simka|kmerdb>\t\t\tThe k-mer counting program [DEFAULT 'simka']\n");
	print("\n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n");
	print("\n\t-v\t\t\tDisplay pyani and/or simka output\n");
	print("\t--help\t\t\tDisplay help\n");
	print("\t--doc\t\t\tDisplay perldoc\n");
	print("\t--version\t\tShow version\n");
	print("\n");

	print("Examples\n");
	
	print("\t>\tperl ki-s.pl -o=res2 -z=examples/fasta-2.zip -m=ani -v\n");

	print("\t>\tperl ki-s.pl -o=res3 -d=examples/fasta-3 -m=kmers\n\n");
}
