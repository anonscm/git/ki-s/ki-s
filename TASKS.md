### Tâches

-	[ ] informations sur les formats utilisés dans les traitements
-	[X]	ajouter aide & dépendences aux scripts R
-	[ ] intégrer la visualisation de communautés (networkViz.r) à Galaxy
-	[X] création d'un script permettant de convertir un fichier de groupes en JSON pour correspondre au format D3.js
-	[ ] intégrer le fichier html de circle packing en sortie

