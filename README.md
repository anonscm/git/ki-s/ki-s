******************************************************************
** KI-S                                              June 2017  **
** Authors : Mariam Bouzid, Tanguy Lallemand, Martial Briand    **
** Copyright or © or Copr. INRA       Licence GNU GPL version 3 **
**                                    Last update : 2020-09-14  **
******************************************************************

Project : https://sourcesup.renater.fr/projects/ki-s
Wiki : https://sourcesup.renater.fr/wiki/ki-s/
Citing KI-S : https://doi.org/10.1101/569640

## **Installation**

Dependancies:
	libxml2-dev
	libcurl4-gnutls-dev
	nodejs
	r-base
	r-cran-magrittr
	r-cran-nmf
	r-cran-irlba
	r-cran-rcurl
	r-cran-xml
	r-cran-htmlwidgets
	r-cran-phangorn
	r-cran-shiny
	blast2
	ncbi-blast+
	libncbi6
	mummer
	pyani
	liblist-moreutils-perl

Download KIS:
	git clone https://git.renater.fr/anonscm/git/ki-s/ki-s.git


## **Basic Use**


### Calculation of distances between strains

	perl ki-s.pl
	
		-z = <zip file containing the genome files>
			or
		-d = <directory containing the genome files>

		-m = <method : 'ani', 'kmers' or 'both'>
	
		-o = <output prefix>
	
		-v <verbose mode>

* Compute ANI
```
		-bl = <blast version : 'ANIb' or 'ANIm'>

		-t = <number of threads>
```
* Compute k-mer

```
		-k = <k-mer length>
```

### Generate circle packing representation of matrix

```
	perl tools/generate_packing.pl

		-in = <matrix file>

		-car = <tabulated characteristics file> 

		-thrs = <'threshold;threshold;threshold'>

		-o = <Output prefix>

```

		
## **Exemples**

### Compute shared K-mers

	`perl ki-s.pl -z gx14.zip -m kmers -k 15 -o gx14k15`


### Compute ANI

	`perl ki-s.pl -z gx14.zip -m ani -bl=ANIb -o gx14anib`


### Compute ANI and shared K-mers

	`perl ki-s.pl -o=res6 -v -z=examples/fasta-6.zip -m=both`


### Generate circle packing representation

	`perl tools/generate_packing.pl -in gx14k15_matrix_kmers.csv -car gx14_infos.tab -o=gx14k15_circ -thrs='0.5;0.8;0.9'`
	`perl tools/generate_packing.pl -in gx14anib_matrix_ani.csv -car gx14_infos.tab -o=gx14anib_circ -thrs='0.95;0.98;0.99'`

### Generate dendrogram

	`Rscript representations/dendro.r <csv matrix file> <output pdf file>`
	
### Network representation

*	With Cytoscape and R
	
	*	Launch Cytoscape 2.8.3:
		`/bin/sh $HOME/Cytoscape_v2.8.3/Cytoscape`
	*	Activate CytoscapeRPC plugin
	
	*	Generate representation:
		`Rscript representations/cliques.r <csv matrix file>`

* With R and shiny
	
	* Lauch R script:
		`Rscript representations/netgen/app.R <csv matrix file> <tabulated characteristics file>`
	* Open this link on your web browser: lien http://localhost:7000

### File manipulation tools

#### Generate adjacency matrix

	`perl tools/adj_mat.pl -in=<csv matrix file> -out=<output csv file> -t=<threshold>`

#### Generate distance matrix
	
	`perl tools/to_dist.pl -in=<csv matrix file> -out=<output csv file>`

#### Convert csv matrix to tabulated matrix
	
	`perl tools/to_tab.pl -in=<csv matrix file> -out=<output tab matrix file> `
	
#### Extract sub matrix
	
	`perl tools/extract_mat.pl -in=<csv matrix file> -out=<output csv matrix file> -li=<file containing the list of samples to be extracted>`

## **Versions history**

### anikpar v4.0 -> ki-s v1.0 01/2018

### v1.1 05/2019
* Add kmer-db choice to calculate percentage of shared k-mers

### v1.2 09/2020
* Add output newick tree (hclust, method average)


