#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)

######################################################################
# 
# + -----------------------------------------------------------------+
# |																		
# |  networkViz.r : Visualisation de réseaux
# | 	voir http://christophergandrud.github.io/networkD3/
# |	Christopher Gandrud, JJ Allaire, Kent Russell, & CJ Yetman
# |
# | 	dependences: networkD3, igraph														 
# +------------------------------------------------------------------+
#
#######################################################################
nomscript <- "simplenetgen.R"
#######################################################################



showHelp <- function (nom)
{
	cat("\n\n")
	cat (nom," - (Simple) Visualisation de communautés.\n")
	cat ("==> http://christophergandrud.github.io/networkD3/\n");
	cat ("\tde Christopher Gandrud, JJ Allaire, Kent Russell, & CJ Yetman\n\n")
	cat ("\n")
	cat ("Dépendences:\n\n")
	cat ("\t- networkD3\n")
	cat ("\t- igraph\n")
	cat ("\n")
	cat ("Utilisation:\n\n")
	cat ("> Rscript ",nom," <matrice [CSV]> <fichier de groupes [TAB]> <fichier de sortie [HTML]> <seuil> \n")
	cat("\n\n")
	quit();
}

n <- length (args)

for (i in (1:n))
{
	if ( args[i] == 'help' || args[i] == 'h' )
	{
		showHelp (nomscript);
	}
}

library(networkD3)
library(igraph)

fichierMatriceCSV <- args[1]
fichierGrpsTAB <- args[2]

fichierHtml <- args[3]
seuil <- args[4]

leCSV <- read.csv(fichierMatriceCSV,header=TRUE,row.names=1,sep=';')
lesgrp <- read.csv(fichierGrpsTAB,header=TRUE,sep='\t',row.names=1)

d <- as.matrix(leCSV)
	
g <- graph.adjacency ( d >= seuil , mode="undirected")
	
	
v <- as_data_frame(g, what="vertices")

e <- as_data_frame(g, what="edges")


i <- 0

for(name in v[['name']])
{	
	grp <- lesgrp[name,"group"]
	v[name,"group"] <- grp
	v[name,"size"] <- 5
	v[name,"id"] <- i
	i <- i+1
}


for (name in v[['name']])
{
	for (j in (1:nrow(e)) ) 	
	{	
		if ( e[j,"from"] == name)
		{
			e[j,"from"] <- v[name,"id"]
		}
		if ( e[j,"to"] == name)
		{
			e[j,"to"] <- v[name,"id"]
		}
	}
}



n <- forceNetwork(Links = e, Nodes = v, Source="from",Target = "to", NodeID = "name",Group = "group", opacity = 10.0, legend = TRUE, zoom = TRUE,bounded=TRUE, opacityNoHover = 1.0, colourScale = JS('force.alpha(1); force.restart(); d3.scaleOrdinal(d3.schemeCategory20);'))
	
saveNetwork(n,fichierHtml)

