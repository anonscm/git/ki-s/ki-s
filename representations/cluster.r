#!/usr/bin/env Rscript
args <- commandArgs (trailingOnly = TRUE)

######################################################################
# 
# + -----------------------------------------------------------------+
# |																		
# |  cluster.r : regroupements de variables corrélées entre elles
# | 	à partir d'un seuil.	
# |
# |  voir http://forge.info.univ-angers.fr/%7Egh/wstat/statgh.php  (gH)
# |																	 
# +------------------------------------------------------------------+
#
#######################################################################
nomscript <- "cluster.r"
#######################################################################

showHelp <- function (nom)
{
	cat("\n\n")
	cat (nom," - Regroupements de variables corrélées entre elles à partir d'un seuil.\n")
	cat ("==> http://forge.info.univ-angers.fr/%7Egh/wstat/statgh.php\n");
	cat ("de (gH)\n")
	cat ("\n")
	cat ("Aucune dépendence.\n")
	cat ("\n")
	cat ("Utilisation: \n")
	cat ("> Rscript ",nom," <matrice d'adjacence [CSV]> <fichier de groupes [TAB]> <seuil>\n")
	cat("\n\n")
	quit();
}


fichierMatriceCSV <- args[1]
fichierGroupesTAB <- args[2]
leSeuil <- args[3]

n <- length (args)

for (i in (1:n))
{
	if ( args[i] == 'help' || args[i] == 'h' )
	{

		showHelp (nomscript);
	}
}



clusterCorTrans <- function(data,seuil=0.8,methode="pearson",detail=FALSE,ret=FALSE,garde=FALSE,...) {

#################################################################

# on fait des groupes de variables toutes corrélées entre elles
# mais avec un regroupement transitif (par propagation)
# si ret=TRUE on renvoie la liste des groupes
# si ret=FALSE et garde=TRUE on renvoie la matrice réduite
# voir aussi clusterCorTrans
# penser aussi à corclust du package klaR

if (missing(data)) {
  cat(" clusterCorTrans : regroupe les colonnes linéairement corrélées par propagation, à plus de rho, fourni en paramètre\n")
  cat("              (valeur par défaut : 0.8)\n")
  cat(" syntaxe : clusterCorTrans(data,seuil=0.8,methode=\"pearson\") \n\n")
  stop()
} ; # fin si

matdata <- data
rho     <- seuil

if (detail)
{
	cat("\nRegroupement transitif de variables fortement corrélées au seuil rho=",rho,"\n")
	cat("pour la corrélation de ",methode,"\n")
}
nbcol     <- ncol(matdata)
nomcol    <- names(matdata)


if (detail)
{
	print(nbcol)
}

colatrait <- rep(1,nbcol)
colvues   <- rep(2,nbcol) # 2 : colonne non vue, 1 en cours, 0 déja traitée
lesgrp    <- rep(0,nbcol) # numéro du groupe

# on passe les colonnes en revue : celles qui sont à plus de rho sont mises
# ensemble (une étoile) ; on met alors colatrait (colonnes à traiter) à 0 et on
# recommence avec les variables du groupe apr tarnsitivité (deux étoiles) jusqu'à saturation
# puis on passe au groupe suivant ...

nb1  <- sum(colatrait==1)
grps <- list()
grp  <- 0 # numéro de groupe
while (nb1>0) {
     ibase    <- 1
     while (ibase<=nbcol) {
       if (colatrait[ibase]==1) {
           itrait <- ibase
           ibase  <- nbcol + 1
       } # fin de si sur colonne à traiter
       ibase <- ibase + 1
     } # fin tant que sur ibase
     grp <- grp + 1
     eff <- 1
     vi <- matdata[,itrait]
     colvues[itrait] <- 0
     if (detail)
     {
		 cat("\nGroupe ",sprintf("%2d",grp)," : ")
		 cat("  ",nomcol[itrait])
	 }
     lesgrp[itrait] <- grp
     vecvars <- nomcol[itrait]
     if (itrait<=nbcol) {
        colatrait[itrait] <- 0

        # premier passage

        eff <- 0
        if (itrait+1<=nbcol) {
        pdvj <- (itrait+1):nbcol
        for (j in pdvj) {
               vj    <- matdata[,j]
               corij <- cor(vi,vj,method=methode,...)
               if (abs(corij)>=rho) {
                  colvues[j]   <- 1
                  if (detail)
                  {
                  	cat("  *",nomcol[j])
                  }
                  vecvars <- c(vecvars,nomcol[j])
                  lesgrp[j] <- grp
                  eff <- eff + 1
               } # fin de si
        } # fin pour j
        } # fin si

        # deuxieme passage (transitivité faible de rho)

        nb2 <- sum(colvues==1)
        jtrait <- nbcol+1
        while (nb2>0) {
          jbase <- 1
          while (jbase<=nbcol) {
            if ( (colvues[jbase]==1) & (colatrait[jbase]==1) ) {
              jtrait <- jbase
              jbase  <- nbcol + 1
              colvues[jtrait]   <- 0
              colatrait[jtrait] <- 0
            } # fin de si sur colonne à traiter
            jbase <- jbase + 1
          } # fin tant que sur jbase

          nb3 <- sum(colvues==1)
          if (jtrait<=nbcol) {
             vjt <- matdata[,jtrait]
             for (j in (1:nbcol)) {
             if (colvues[j]==2) {
                 if (colatrait[j]==1) {
                  vj    <- matdata[,j]
                  corij <- cor(vjt,vj,method=methode,...)
                  if (abs(corij)>=rho) {
                     colvues[j]   <- 1
                     if (detail)
                     {
                     	cat("  **",nomcol[j])
                     }
                     lesgrp[j] <- grp
                     vecvars <- c(vecvars,nomcol[j])
                  } # fin de si
                 } # fin de si
             } # fin de si
             } # fin pour j
          } # fin si
          nb2 <- sum(colvues==1)
        } # fin de tant que
     } # fin de si on a au moins une colonne à traiter

     nb1 <- sum(colatrait==1)
     grps[[grp]] <- vecvars
} # fin de tant que
if (detail) 
{
	cat("\n")
}
# msie en forme des résultats

retenue <- rep(0,nbcol)
gard   <- rep(0,grp)
for (igrp in (1:grp)) {
  icol  <- 1
  igard <- 0
  while (icol<=nbcol) {
    if (lesgrp[icol]==igrp) {
      igard <- icol
      icol  <- nbcol + 1
    } # fin si
    icol <- icol + 1
  } # fin tant que
  gard[igrp]     <- igard
  retenue[igard] <- igrp
} # fin pour
mret <- cbind(lesgrp,retenue)
row.names(mret) <- names(matdata)
colnames(mret) <- c("Groupe","Variable retenue")

if (detail) {

     cat("\n")
     cat("Numéros de groupe de corrélation et variable retenue : \n")
     print(mret)
     cat("\n")
     cat("On retient ",grp," variables :")

} # fin si

mgarde <- matdata[,gard]

if (detail) 
{
	cat("\nRésumé des dimensions\n")
	cat(" données initiales : ",sprintf("%5d",dim(matdata)),"\n")
	cat(" données finales   : ",sprintf("%5d",dim(mgarde)),"\n\n")
}

if (ret) {
  return(grps)
} else {
  if (garde) {
    return(mgarde)
  } # fin si
} # fin si


} # fin de fonction clusterCorTrans

leCSV <- read.csv(fichierMatriceCSV,header=TRUE,row.names=1,sep=';')

lesgrp <- clusterCorTrans (data=leCSV,seuil=leSeuil,methode="pearson",detail=FALSE,ret=TRUE,garde=FALSE)

nomcluster <- paste("\t", sub ("\\.(.*)", '', basename(fichierMatriceCSV) ),"_", leSeuil, sep='' )

write(nomcluster,fichierGroupesTAB,append=FALSE)

i <- 1
for (groupe in lesgrp)
{
	for (membre in groupe)
	{
		row <- paste(membre,"\tG_",i,"_",leSeuil,sep="")
		write(row,fichierGroupesTAB,append=TRUE)
	}
	i <- i+1
}

