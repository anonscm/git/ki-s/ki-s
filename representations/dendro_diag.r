#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)

######################################################################
# 
# + -----------------------------------------------------------------+
# |																		
# |  dendro.r : dendrogramme en PDF à partir d'une matrice de
# | 	distance.										 
# |																	 
# +------------------------------------------------------------------+
#
#######################################################################
nomscript <- "dendro.r"
#######################################################################

fichierMatriceDistCSV <- args[1]
fichierDendroPDF <- args[2]

showHelp <- function (nom)
{
	cat("\n\n")
	cat (nom," - Dendrogramme en PDF à partir d'une matrice de distance.\n")
	cat ("\n")
	cat ("Aucune dépendence.\n\n")
	cat ("\n")
	cat ("Utilisation: \n\n")
	cat ("\t > Rscript ",nom," <matrice de distance [CSV]> <nom dendrogramme [PDF]>\n")
	cat("\n\n")
	quit();
}

n <- length (args)

for (i in (1:n))
{
	if ( args[i] == 'help' || args[i] == 'h' )
	{
		showHelp (nomscript);
	}
}

pdf(fichierDendroPDF,width=40,height=30)
cr3 <- as.matrix(read.table(fichierMatriceDistCSV, sep=";", header=TRUE, row.names=1))
lcr3 <- - log(cr3)
lcr3 <- lcr3*100
cr3_distance <- as.dist(lcr3)
dendro_cr3 <- hclust(cr3_distance, method="ward.D2")
plot(dendro_cr3, main="Shared Kmers dendrogram (hclust, ward.D2)", cex = 2 , sub = fichierDendroPDF, xlab = paste("Complete clusterization of ",fichierMatriceDistCSV))
