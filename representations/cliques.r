#!/usr/bin/env Rscript
args <- commandArgs (trailingOnly=TRUE)

######################################################################
# 
# + -----------------------------------------------------------------+
# |																		
# |  cliques.r : représentation graphique avec Cytoscape à partir 
# |	d'une matrice de distance.										 
# |																	 
# +------------------------------------------------------------------+
#
#######################################################################
nomscript <- "cliques.r"
#######################################################################

fichierMatriceAdj <- args[1]

showHelp <- function (nom)
{
	cat("\n\n")
	cat (nom," - Représentation graphique avec Cytoscape à partir d'une matrice.\n")
	cat ("==> http://rcytoscape.systemsbiology.net/versions/current/index.html\n");
	cat ("\tde pshannon\n\n")
	cat ("\n")
	cat ("Dépendences:\n\n")
	cat ("\t- igraph\n")
	cat ("\t- plyr\n")
	cat ("\t- RCytoscape\n")
	cat ("\t- XMLRPC\n")
	cat ("\n")
	cat ("Utilisation: \n\n")
	cat ("- lancer Cytoscape 2.8.3\n")
	cat ("- activer le plugin CytoscapeRPC (et cocher enable XMLRPC)\n")
	cat ("- exécuter la commande: \n")
	cat ("\t > Rscript ",nom," <matrice d'adjacence [CSV]> \n")
	cat ("- une fenetre au nom du fichier CSV devrait apparaitre dans Cytoscape\n")
	cat ("- pour obtenir une jolie représentation faire dans le menu Cytoscape: \n")
	cat ("\t Layout > Cytoscape Layouts > Attribute Circle Layout > degre\n")
	cat("\n\n")
	quit();
}

n <- length (args)

for (i in (1:n))
{
	if ( args[i] == 'help' || args[i] == 'h' )
	{
		showHelp (nomscript);
	}
}

# Chargement de igraph

library ('igraph')
library ('plyr')

# Lecture du fichier tab 

leCSV <- read.csv(fichierMatriceAdj,header=TRUE,row.names=1,sep=';')

uneMatrice <- as.matrix (leCSV)

leGraphe <- graph.adjacency (adjmatrix=uneMatrice, mode="undirected",diag=FALSE)

# Calcul du nombre de sommets et d'arêtes

nbSommets <- vcount (leGraphe)
nbAretes <- ecount (leGraphe)

# Affichage du graphe

print (leGraphe)

# Chargements de RCytoscape

library ('RCytoscape')

leGraphe.cyt <- igraph.to.graphNEL (leGraphe)

leGraphe.cyt <- initNodeAttribute (leGraphe.cyt, 'degre', 'numeric', 0)

leGraphe.cyt <- initEdgeAttribute (leGraphe.cyt, "weight", 'numeric', 0)

leGrapheW <- new.CytoscapeWindow (paste("Graph for ",fichierMatriceAdj), graph = leGraphe.cyt, overwriteWindow = TRUE)

displayGraph (leGrapheW)

cy <- CytoscapeConnection ()


