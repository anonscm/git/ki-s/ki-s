#include "cluster.h"


template <class T>
std::ostream & operator << (std::ostream & os, const cluster <T> & _)
{	
	if ( _.empty () )
	{
		os << "{}";
	}
	else
	{
		os << "{ ";
		for (auto i = _.begin(); i != _.end(); ++i)
		{
			os << (*i) << " ";
		}
		os << "}";
	}
	
	return (os);
}



template typename std::ostream & operator << < std::string > (std::ostream &, const cluster <std::string> &);
