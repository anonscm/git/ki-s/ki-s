#ifndef defs_h
#define defs_h

#include <string>
#include <vector>
#include <cmath>
#include <algorithm>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <exception>
#include <set>
#include "cluster.h"


typedef std::map < std::string,  std::map < std::string, std::string > > matrix;

// STRING

std::vector < std::string > split (const std::string & str, const std::string & delim);

// STAT 

double sum(const std::vector < double > & v);
double mean(const std::vector < double > & v);
double sqsum (const std::vector < double > & v);
double stdev (const std::vector < double > & nums);
std::vector < double > operator-( const std::vector <double>  & a , double b);
std::vector < double > operator* (const std::vector<double>  & a,const std::vector<double> & b);
double pearsoncoeff (const std::vector<double>  & X,const std::vector < double > & Y);

// VECT

std::vector <double> to_double (const std::vector <std::string> & v);
template <class T> void rep (std::vector <T> & v, const T & t);
std::vector < std::string > get_data_at_column (const std::string & colname, const std::vector < std::string > & colnames, const matrix & m);

// MISC.
std::ostream & tabular (std::ostream & os, size_t nb);

// CLUSTERS
std::vector < cluster <std::string> > clusterize (const std::vector < std::string > & nomcol, const matrix & matdata, const double & rho);


#endif
