#include <cstdlib>
#include "csv.h"
#include <initializer_list>
#include <cstring>
#include <unistd.h>

int main (int argc, char * argv [])
{
	
	for (int i = 0; i < argc; ++i)
	{
		if ( (strcmp (argv[i],"help") == 0)  or (strcmp (argv[i],"h") == 0) )
		{
			std::cout << std::endl;
			std::cout << basename(argv[0]) << " - Regroupements de variables corrélées entre elles à partir d'un seuil." << std::endl;
			std::cout << std::endl;
			std::cout << "Utilisation: " << std::endl << std::endl;
			std::cout << "\t./bin/clust <fichier matrice distance (CSV)> <fichier groupes (TAB)> <seuil> " << std::endl;
			std::cout << std::endl;
			
			return (EXIT_SUCCESS);

		} 
	}

	try
	{
		if ( argc == 4 )
		{
			double threshold (std::stof(argv[3]));
			std::string input (argv[1]);
			std::string output (argv[2]);
			csv * _ = csv::load (input);
			
			_->save (output, threshold);
			
			delete (_);		
			return (EXIT_SUCCESS);
			
		}
		else
		{
			throw ("Wrong number of arguments. Exiting");
		}
	}
	catch ( const std::exception & e)
	{
		std::cerr << "Exception caught: " << e.what() << std::endl;
		exit (EXIT_FAILURE);
	}

}
