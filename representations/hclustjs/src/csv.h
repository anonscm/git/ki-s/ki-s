#ifndef csv_h
#define csv_h

#include "tools.h"

class csv 
{
	private:
	
		std::string m_filename;

		std::vector < std::string > m_rows;
		std::vector < std::string > m_columns;
	
		matrix m_values;
	
		
	public:

		csv (const std::string  & filename,
			 const std::vector < std::string > & rows,
			 const std::vector < std::string > & columns,
			 const matrix & values):
			 m_filename (filename),
			 m_rows (rows),
			 m_columns (columns),
			 m_values (values)
		{}
	
		csv ( const csv & _ ):
			m_filename (_.m_filename),
			m_rows (_.m_rows),
			m_columns (_.m_columns),
			m_values (_.m_values)
		{}

		static csv * load (const std::string & filename, const std::string & delim = ";") throw (std::exception&);	


		const std::string & get_filename () const
		{
			return (m_filename);
		}

		const matrix & get_values () const
		{
			return (m_values);
		}
	
		const std::vector < std::string >  & get_rows() const
		{
			return (m_rows);
		}
		
		const std::vector < std::string >  & get_columns() const
		{
			return (m_columns);
		}

		~csv ()
		{}
		
		void save (const std::string & filename, const double & rho ) const throw (std::exception& );
	
		csv * extract (const std::vector < std::string > & subrows, const std::vector < std::string> & subcols);
		
		void hclust (const double & rho1, const double & rho2);
};

std::ostream & operator << (std::ostream & os, const csv & _);


#endif
