#include "tools.h"



std::vector < std::string > split (const std::string & str, const std::string & delim)
{	
	std::vector < std::string > tokens;
	size_t start = str.find_first_not_of (delim);
	size_t last = start;

	while ( start != std::string::npos )
	{
		last = str.find (delim, start);
		tokens.push_back (str.substr (start, last-start));
		start = str.find_first_not_of (delim, last);
	}

	return (tokens);
}

double sum (const std::vector<double> & v)
{
	return (std::accumulate (v.begin(), v.end(), 0.0) );
}

double mean (const std::vector <double>  & v)
{
	return ( (double) sum(v) / (double) v.size());
}

double sqsum(const std::vector <double> & v)
{
	return ( std::accumulate (v.begin(), v.end(), 0.0,[] (double acc, double a) { return (acc + std::pow (a,2) );}));
}

double stdev (const std::vector<double> & nums)
{
	double N = nums.size();
	return pow(sqsum(nums) / N - pow(sum(nums) / N, 2), 0.5);
}

std::vector<double> operator-(const std::vector <double> & a, double b)
{
	std::vector<double> retvect;
	for (size_t i = 0; i < a.size(); i++)
	{
		retvect.push_back((double) (a.at(i) - b));
	}
	return retvect;
}

std::vector <double> operator*(const std::vector<double> & a, const std::vector <double> & b)
{
	std::vector < double> retvect;
	for (size_t i = 0; i < a.size() ; i++)
	{
		retvect.push_back((double) (a.at(i) * b.at(i)));
	}
	return retvect;
}

double pearsoncoeff (const std::vector <double> & X,  const std::vector<double> & Y)
{
	return sum((X - mean(X))*(Y - mean(Y))) / (X.size()*stdev(X)* stdev(Y));
}


std::vector <double> to_double (const std::vector <std::string> & v)
{
	std::vector <double> d ;
	
	for (auto i = v.begin(); i != v.end(); ++i)
	{
		d.push_back (std::stof (*i));
	}
	
	return (d);
}

std::vector < std::string > get_data_at_column (const std::string & colname, const std::vector < std::string > & colnames, const matrix & matdata)
{	
	std::vector < std::string > data_at_column;
	
	for (auto  i : colnames )
	{
		for (auto j : colnames)
		{
			if ( colname == j )
			{
				data_at_column.push_back (matdata.at(i).at(j));
			}
		}
	}
	
	return (data_at_column);
}

template <class T>
void rep (std::vector <T> & v, const T & t)
{
	for (typename std::vector<T>::iterator i = v.begin(); i != v.end(); ++i)
	{
		(*i) = t;
	}
}

std::ostream & tabular (std::ostream & os, size_t nb)
{
	for (size_t i = 0; i < nb; ++i)
	{
		os << "\t";
	}
	return os;
}


std::vector < cluster <std::string> > clusterize (const std::vector < std::string > & nomcol, const matrix & matdata, const double & rho)
{
	size_t nbcol = nomcol.size();
	
	std::vector < cluster <std::string> > clusters ;
	
	std::vector <size_t> colatrait (nbcol), colvues (nbcol), lesgrp (nbcol) ;
	rep < size_t > (colatrait, 1); 
	rep < size_t > (colvues,2);
	rep < size_t > (lesgrp,0);
	
	
	

	size_t nb1 = std::accumulate (colatrait.begin(),colatrait.end(),0, [] (size_t acc, size_t a) { return ( ( a == 1) ? (acc+1) : (acc));});
	std::map <size_t, std::vector < std::string > > grps;
	
	size_t grp = 0;
	
	while ( nb1 > 0 )
	{
		size_t ibase = 0;
		size_t itrait;
		while ( ibase < nbcol )
		{
			if (colatrait.at(ibase) == 1)
			{
				itrait = ibase;
				ibase = nbcol;
			} // fin de si
			++ibase;
		} // fin de tant que
		
		++grp;
		size_t eff = 1;
		std::vector < double > vi (to_double (get_data_at_column (nomcol.at(itrait),nomcol,matdata)));

		
		colvues[itrait] = 0;
			
		lesgrp[itrait] = grp;
		
		
		std::vector < std::string > vecvars ;
		vecvars.push_back(nomcol.at(itrait));
		
		if (itrait < nbcol )
		{
			colatrait[itrait] = 0;
			eff = 0;
		
			if ( itrait+1 < nbcol )
			{
				std::vector <size_t> pdvj;
 		 		for (size_t k = itrait+1; k < nbcol; ++k)
 		 		{
 		 			pdvj.push_back (k);
 		 		} // fin pour k
 		 		
 		 
				for (size_t j : pdvj)
				{
					std::vector < double > vj (to_double (get_data_at_column (nomcol.at(j),nomcol,matdata)));
					double corij = pearsoncoeff (vi,vj);
					
					if ( corij >= rho )
					{
						colvues [j] = 1;
						
						vecvars.push_back (nomcol.at(j));
						
						
						lesgrp [j] = grp;
						++eff;
					}// fin de si
					
					
				} // fin pour j
			} // fin si
			
		
			size_t nb2 = std::accumulate (colvues.begin(),colvues.end(),0, [] (size_t acc, size_t a) { return ( ( a == 1) ? (acc+1) : (acc));});	
			size_t jtrait = nbcol;
		
			while ( nb2 > 0 )
			{
				size_t jbase = 0;
				while ( jbase < nbcol )
				{
					if ( (colvues[jbase] == 1) and (colatrait[jbase] == 1) )
					{
						jtrait = jbase ;
						jbase = nbcol;
						colvues[jtrait] = 0;
						colatrait[jtrait] = 0;
					} // fin de si
					++jbase;
				} // fin de tant que
				
				
				if (jtrait < nbcol )
				{
					std::vector < double > vjt (to_double (get_data_at_column (nomcol.at(jtrait),nomcol,matdata)));
					for (size_t j = 1; j < nbcol; ++j)
					{
						if (colvues[j] == 2 )
						{
							if (colatrait[j] == 1 )
							{
								std::vector < double > vj (to_double (get_data_at_column (nomcol.at(j),nomcol,matdata)));
								double corij = pearsoncoeff (vjt,vj);
								if ( corij >= rho)
								{
									
									colvues[j] = 1;
									lesgrp[j] = grp;
									vecvars.push_back (nomcol.at(j));
								} // fin de si
								
							} // fin de si
							
						} // fin de si
						
					} // fin de pour
				
				} // fin de si
				
					nb2 = std::accumulate (colvues.begin(),colvues.end(),0, [] (size_t acc, size_t a) { return ( ( a == 1) ? (acc+1) : (acc));});
			} // fin de tant que
				
					
		} // fin de si on a au moins une colonne à traiter
		
		nb1 = std::accumulate (colatrait.begin(),colatrait.end(),0, [] (size_t acc, size_t a) { return ( ( a == 1) ? (acc+1) : (acc));});
     	grps[grp] = vecvars;
	
		std::ostringstream oss ;
		oss << "G_" << grp << "_" << rho ;
		
		clusters.push_back (cluster <std::string> (oss.str(),vecvars));
	}

	
	 return (clusters);
}



// TEMPLATE DEFINITION

template void rep <size_t> (std::vector < size_t > & , const size_t &) ;
