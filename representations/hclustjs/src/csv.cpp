#include "csv.h"



csv * csv::load (const std::string & filename, const std::string & delim) throw (std::exception& )
{
	std::ifstream f (filename);

	if (f)
	{
		
		std::string line;
	
		std::getline (f,line);
		
		std::vector < std::string > columns (split (line,delim));
		std::vector < std::string > rows; 
		matrix values;
		
		while ( std::getline (f,line) )
		{
			
			std::vector < std::string > tokens (split (line,delim));
			
			std::string row (tokens.at(0));
			rows.push_back (row);
			
			for (size_t i = 1; i < tokens.size(); ++i)
			{
				values[row][columns.at(i-1)] = tokens.at(i);
			}
				
		}
		
		std::sort (rows.begin(), rows.end());
		std::sort (columns.begin(), columns.end());
		
		return (new csv (filename, rows, columns, values));
		
	}
	else
	{
		throw ("Error with the input file "+filename+". Exiting.");
	}
}

csv * csv::extract (const std::vector < std::string > & subrows, const std::vector < std::string > & subcols)
{
	matrix subvalues;
	
	for (auto r : subrows)
	{
		for (auto c : subcols)
		{
			subvalues[r][c] = m_values.at (r).at (c);
		}
	}
	
	return (new csv (m_filename, subrows, subcols, subvalues));
}

std::ostream & operator << (std::ostream & os, const csv & _)
{
	for (const std::string & i : _.get_columns() )
	{
		os << ";" << i;
	}

	os << std::endl;

	for (const std::string & i : _.get_rows() )
	{
		os << i ;
		for (const std::string & j : _.get_columns() )
		{
			os << ";" << _.get_values().at(i).at(j);
		}
		os << std::endl;
	}

	return (os);
		
}

void csv::hclust (const double & rho1, const double & rho2)
{
	std::vector < cluster <std::string> > clusters (clusterize (m_columns, m_values, rho1));
	for (auto c : clusters)
	{	
		std::vector <std::string> rows (c.begin(), c.end());
		std::vector <std::string> cols (rows);
		csv * sub = this->extract (rows,cols);
		
		std::vector < cluster <std::string> > subclust (clusterize (sub->get_columns(), sub->get_values(), rho2));
		
		for (auto sc : subclust)
		{
			std::cout << sc << std::endl;
		}
		
	}
	std::cout << std::endl;
}




void csv::save (const std::string & filename, const double & rho) const throw (std::exception&)
{
	std::ofstream f (filename);
	
	if (f)
	{
		std::vector < cluster <std::string> > clusters (clusterize (m_columns, m_values, rho));
	
		f << "\t" << "cluster_" << rho << std::endl;
		for (auto c : clusters)
		{
			for (auto member : c )
			{
				f << member << "\t" << c.get_name() << std::endl;
			}
		}
	}
	else
	{
		throw ("Error with the input file "+filename+". Exiting.");
	}
}



