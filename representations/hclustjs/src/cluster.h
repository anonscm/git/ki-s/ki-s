#ifndef cluster_h
#define cluster_h

#include <set>
#include <iostream>
#include <algorithm>
#include <string>

template <class T>
class cluster : public std::set <T> 
{
	private:
		
		std::string m_name;
		
	public:
		
		cluster (const std::string & name, const std::initializer_list <T> & list):
			std::set <T>(),
			m_name (name)
		{
			for (const T & e : list)
			{
				this->insert (e);
			}
		}
		
		cluster (const std::string & name, const std::vector <T> & vect):
			std::set <T> (),
			m_name (name)
		{
			for (const T & e : vect)
			{
				this->insert (e);
			}
		}
	
		const std::string & get_name () const
		{
			return (m_name);
		}	
};



template <class T>
std::ostream & operator << (std::ostream & os, const cluster <T> & _);

#endif
