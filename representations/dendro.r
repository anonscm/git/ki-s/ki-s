#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)

######################################################################
# 
# + -----------------------------------------------------------------+
# |																		
# |  dendro.r : dendrogram from percentage of shared K-mers	 
# |																	 
# +------------------------------------------------------------------+
#
#######################################################################
nomscript <- "dendro.r"
#######################################################################

fichierMatriceDistCSV <- args[1]
fichierDendroPDF <- args[2]
fichierDendroNewick <- args[3]

showHelp <- function (nom)
{
	cat("\n\n")
	cat (nom," - Dendrogram from percentage of shared K-mers.\n")
	cat ("\n")
	cat ("Dependancy : ape.\n\n")
	cat ("\n")
	cat ("Usage: \n\n")
	cat ("\t > Rscript ",nom," <percentage of shared K-mers matrice [CSV]> <Output pdf dendrogram [PDF]> <Output newick tree [NWK]>\n")
	cat("\n\n")
	quit();
}

n <- length (args)

for (i in (1:n))
{
	if ( args[i] == 'help' || args[i] == 'h' )
	{
		showHelp (nomscript);
	}
}

pdf(fichierDendroPDF,width=40,height=30)
myMAT <- as.matrix(read.table(fichierMatriceDistCSV, sep=";", header=TRUE, row.names=1))
myMAT <- (1-myMAT)*100
myMAT_distance <- as.dist(myMAT)
dendro_myMAT <- hclust(myMAT_distance, method="average")
plot(dendro_myMAT, main="Shared Kmers dendrogram (hclust, ward.D2)", cex = 2 , sub = fichierDendroPDF, xlab = paste("Complete clusterization of ",fichierMatriceDistCSV))
library("ape")
my_tree <- as.phylo(dendro_myMAT)
my_unrooted_tree <- unroot(my_tree)
write.tree(phy=my_unrooted_tree, file=fichierDendroNewick)
