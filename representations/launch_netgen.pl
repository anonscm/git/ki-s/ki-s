#!/usr/bin/perl -w
#

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Cwd qw(abs_path realpath getcwd);

my $wd = getcwd();
my $pwd = dirname(realpath($0));

my ($mat, $grp, $h);

my $script = $pwd."/netgen/app.R";
my $output = $pwd."/netgen/index.html";

GetOptions
(
	"mat=s" => \$mat,						# csv file input
	"grp=s" => \$grp,						# csv file input
	"help" => \$h							# show help
);


if ($h)
{
	&help();
	exit;
}

unless ($mat and $grp)
{
	print("\nMissing arguments.. ¯\\_(ツ)_/¯ \n Exiting.\n");
	print("See help with `perl ".basename($0)." --help` \n\n");
	exit;
}


`cp $output $wd/output.html`;
`Rscript $script $mat $grp`;

exit;




=head2	function help

	Title			: help
	Usage			: &help()
	Prerequisite		: none
	Function		: Displays the help 
	Returns			: none
	Args			: none


=cut

sub help
{
	print("\n");
	print(basename($0)." - Launches the network visualisation tool R\n"); 
	print("\n");
	print("\t-mat=\t\t\t<csv file>\t\t\tThe input csv matrix  \n");
	print("\t-grp=\t\t\t<json file>\t\t\tThe input groups file\n");
	print("\n");
	print("\n");
	
	print("DOCUMENTATION & HELP\n\n");
	print("\t--help\t\t\tDisplay help\n");
	print("\t-v \t\t\tVerbose mode\n");
	print("\n");

	print("Example\n");
	
	print("\t>\tperl tools/launch_netgen.pl -grp=grp.csv -mat=mat.csv\n\n");
	

}

