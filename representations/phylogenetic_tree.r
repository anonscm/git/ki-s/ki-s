#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)

######################################################################
# 
# + -----------------------------------------------------------------+
# |																		
# |  phylogenetic_tree.r : arbre phylogénétique en PDF à partir d'une 
# | 	matrice de distance.										 
# |		
# |		Schliep K.P.2011. phangorn: phylognetic analysis in R.
# |			Bioinformatics, 27 (4) 592-593	
# |														 
# +------------------------------------------------------------------+
#
#######################################################################
nomscript <- "phylogenetic_tree.r"
#######################################################################

fichierMatriceDistCSV <- args[1]
fichierArbrePDF <- args[2]

showHelp <- function (nom)
{
	cat("\n\n")
	cat (nom," - Arbre phylogénétique en PDF à partir d'une matrice de distance.\n")
	cat ("==> phangorn: phylognetic analysis in R. Bioinformatics, 27 (4) 592-593\n")
	cat ("\tde Schliep K.P.\n\n")
	cat ("Dépendences:\n\n")
	cat ("- phangorn\n")
	cat ("- ape\n")
	cat ("\n")
	cat ("Utilisation: \n\n")
	cat ("\t > Rscript ",nom," <matrice de distance [CSV]> <nom arbre [PDF]>\n")
	cat("\n\n")
	quit();
}

n <- length (args)

for (i in (1:n))
{
	if ( args[i] == 'help' || args[i] == 'h' )
	{
		showHelp (nomscript);
	}
}

require ("phangorn")

pdf(fichierArbrePDF,width=40,height=30)

d <- as.matrix(read.table(fichierMatriceDistCSV, sep=";", header=TRUE, row.names=1))

tree <- upgma (d,method="complete")

plot(tree,main="UPGMA clustering", cex = 2 , sub = fichierArbrePDF)
